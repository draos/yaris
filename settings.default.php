<?php

/******************************************************************************************
 * Settings
 ******************************************************************************************/
$settings = (object)[
  // Cron token (required)
  "cron_token" => '',
  // Database settings (required)
  "database" => (object)[
    "name" => '',
    "user" => '',
    "password" => '',
  ],
  // Global settings
  "global" => (object)[
    // Language settings
    "language" => 'en',
    // Locales to use
    "locale" => ['de_DE', 'de_DE.UTF-8'],
    // Timezone
    "timezone" => 'Europe/Berlin',
  ],
  // Styles to include
  "style" => ['css/main.css', 'css/fontawesome.css', 'css/weathericons.css'],
  // Module specific settings
  //   Name of the module must match the file 'modules/{module_name}.php'
  // Pre-defined options:
  //   active .. Set false to deactivate the module. Default true
  //   cache .. Creates the caching directory under 'cache/{module_name}/'
  //   class .. Name of the module class. Required.
  "modules" => (object)[
    // Settings for the calendar module
    "calendar" => (object)[
      "active" => FALSE,
      // php class
      "class" => 'CalendarModule',
      // Maximum events shown
      "maximum" => 11,
      // Maximum days
      "days" => 31,
      // Connection settings (required)
      "url" => '',
      "username" => '',
      "password" => '',
      "calendar" => '',
    ],
    // Settings for the Anime module
    "anime" => (object)[
      "active" => FALSE,
      // php class
      "class" => 'AnimeModule',
      // Filter by languages. Empty list or comment out if all
      "language" => ['en'],
      // Maximum number of entries shown
      "maximum" => 12,
    ],
    // Settings for the Manga module
    "anime" => (object)[
      "active" => FALSE,
      // php class
      "class" => 'MangaModule',
      // Maximum number of entries shown
      "maximum" => 12,
    ],
    // Settings for the xkcd module
    "xkcd" => (object)[
      "active" => FALSE,
      // php class
      "class" => 'XKCDModule',
      // Cache directory for the images
      "cache" => TRUE,
    ],
    // Settings for the OpenWeatherMap module
    "openweathermap" => (object)[
      "active" => FALSE,
      // php class
      "class" => 'OpenWeatherModule',
      // API key (required)
      "api_key" => '',
      // ID of the city (required)
      "city_id" => 0,
      // Show the trend for the next given hours
      "next_hours" => 72,
    ],
    // Settings for Guild Wars 2 module
    "guildwars2" => (object)[
      "active" => FALSE,
      // php class
      "class" => 'Gw2Module',
      // Cache directory for the icons
      "cache" => TRUE,
      // Maximum number of shown items per category
      "maximum" => 6,
      // Maximum number of shown blc collections
      "maximum_blc" => 12,
      // Region eu or na (default is eu)
      "region" => "eu",
      // Groups of the watchlist
      "groups" => (object)[
        "Plants" => [
          12134, 12135, 12142, 12147, 12161, 12162, 12163, 12234, 12236, 12238,
          12241, 12243, 12244, 12246, 12247, 12248, 12253, 12254, 12255, 12329,
          12330, 12331, 12332, 12333, 12334, 12335, 12336, 12341, 12342, 12504,
          12505, 12506, 12507, 12508, 12511, 12512, 12531, 12532, 12534, 12535,
          12536, 12537, 12538, 12546, 12547, 36731, 66524, 73096, 73113, 73504,
          82866,
        ],
      ],
    ],
  ],
];
