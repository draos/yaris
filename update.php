<!DOCTYPE html>
<html>
<head>
  <title>Update</title>
</head>
<?php
  require_once('settings.php');
  require_once('modules/base.php');

  $token = isset($_GET['token']) ? $_GET['token'] : NULL;
  if (!isset($settings->cron_token) || $token === $settings->cron_token) {
    if (isset($settings->modules))
      foreach ($settings->modules as $name => $module) {
        if ($module->active ?? TRUE)
          try {
            require_once("modules/{$name}.php");
            (new $module->class())->upgrade();
          } catch (Exception $e) {
            continue;
          }
      }
  }
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="container">
    <table id="modules">
<?php
    $base = new \BaseModule();
    $res = $base->query('SELECT * FROM "modules" ORDER BY name');
    while ($row = $res->fetch_object()) {
      $dt = new \DateTime($row->timestamp, new \DateTimeZone("UTC"));
      $tz = $base->global('timezone');
      if (isset($tz))
        $dt->setTimezone(new \DateTimeZone($tz));?>
      <tr>
        <td class="name"><?php echo $row->name;?></td>
        <td class="timestamp"><?php echo $dt->format('H:i:s d-m-Y');?></td>
      </tr>
<?php }?>
    </table>
  </div>
</body>
</html>
