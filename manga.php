<!DOCTYPE html>
<html>
<meta http-equiv="refresh" content="60"/>
<head>
  <title>Manga</title>
</head>

<?php
  require_once('modules/manga.php');

  $module = new \MangaModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);

  $mode = NULL;
  switch ($_GET['mode'] ?? '') {
    case 'manga':
      $mode = $_GET['mode'];
      break;
  }
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="submenu">
    <a class="fas fa-sort-amount-down" href="manga.php"></a>
    <a class="fas fa-sort-alpha-down" href="manga.php?mode=manga"></a>
  </div>

  <div id="container">
    <table id="animes">
<?php if ($mode === 'manga') {
  foreach ($module->get_mangas() as $manga) {?>
    <tr><td class="manga"><?php echo $manga;?></td></tr>
<?php }} else {
  echo "      <col width=\"25%\"/><col width=\"15%\"/><col width=\"60%\"/>\n";
  foreach ($module->get_latest() as $item) {?>
    <tr>
      <td class="manga"><?php echo $item->manga;?></td>
      <td class="nr"><?php echo "[{$item->nr}]";?></td>
      <td class="chapter"><?php echo $item->chapter;?></td>
    </tr>
<?php }}?>
    </table>
  </div>
</body>
</html>
