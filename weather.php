<!DOCTYPE html>
<html>
<meta http-equiv="refresh" content="60"/>
<head>
  <title>Weather</title>
</head>

<?php
  require_once('modules/openweathermap.php');

  // Build the api object
  $module = new \OpenWeatherModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);
  $city = $module->city();

  /***********************************************************************************************
   * Render a day
   ***********************************************************************************************/
  function render_day($day, $data) {
    // Beaufort scale: v = 0.836 B^(1.5)
    $bf = max(0, min(12, round(pow($data->wind_speed_max / 0.836, 0.66667))));
    $wind_speed = 3.6 * $data->wind_speed_max;
    if ($bf >= 12)
        echo '<span class="alert wi wi-hurricane-warning"></span>';
      else if ($bf >= 10)
        echo '<span class="alert wi wi-storm-warning"></span>';
      else if ($bf >= 8)
        echo '<span class="alert wi wi-gale-warning"></span>';
      else if ($bf >= 6)
        echo '<span class="alert wi wi-small-craft-advisory"></span>';?>

      <div class="date"><?php echo $day;?></div>
      <div class="icon">
        <span class="wi wi-<?php echo $data->condition;?>"></span>
      </div>
      <div class="temperature">
        <span class="max-temperature">
          <?php echo round($data->temp_max);?><span class="wi wi-celsius"></span>
        </span>
        <span class="min-temperature">
          <?php echo round($data->temp_min);?><span class="wi wi-celsius"></span>
        </span>
      </div>
      <div class="additional">
        <div class="humidity">
          <?php echo round($data->humidity);?> <span class="wi wi-humidity"></span>
        </div>
        <div class="rain">
          <span class="wi wi-qpf"></span>
          <?php echo round($data->rain + $data->snow);?> mm
        </div>
        <div class="wind-speed">
          <span class="wi wi-wind-<?php echo $bf;?>"></span>
          <?php echo round($wind_speed);?> km/h
        </div>
        <div class="sun">
          <span class="wi wi-sun-horizon"></span>
          <?php echo $data->sunrise;?>-<?php echo $data->sunset;?>
        </div>
      </div>
<?php }?>

<?php build_styles()?>

<body>
<?php build_menu();?>

  <div id="location">
<?php if (isset($city->country, $city->name)) {?>
    <span class="country"><?php echo $city->country;?></span>
    <span class="city"><?php echo $city->name;?></span>
<?php }?>
  </div>

<?php
  // Get the current timezone
  $tz = $module->global('timezone');
  $tz = new \DateTimeZone(isset($tz) ? $tz : 'UTC');

  // Get the current date
  $date = new \DateTime("", $tz);
  $data = $module->daily($date);
  $day = strftime('%a', $date->format('U'));
?>
  <div id="container" class="weather">
    <div class="today"><?php if ($data) render_day($day, $data);?></div>
<?php
    $one_day = new \DateInterval('P1D');
    // Show the next 5 days
    for ($i = 1; $i < 5; $i++) {
      $date->add($one_day);
      $d = $date->format('Y-m-d');
      if ($data = $module->daily($date)) {
        $bf = max(0, min(12, round(pow($data->wind_speed_max / 0.836, 0.66667))));
        $day = strftime('%a', $date->format('U'));
        $wind_speed = 3.6 * $data->wind_speed_max;?>

      <div class="daily"><?php render_day($day, $data); ?></div>
<?php }}?>
  </div>
</body>
</html>
