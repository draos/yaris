<?php
require_once('settings.php');

function api_ok() {
  return (object)["status" => "ok"];
}

function startswith(string $value, string $sub) {
  return substr($value, 0, strlen($sub)) === $sub;
}

function endswith(string $value, string $sub) {
  $len = strlen($sub);
  return substr($value, -$len, $len) == $sub;
}

/******************************************************************************************
 * Build the menu using the settings
 ******************************************************************************************/
function build_menu($current=NULL) {
  global $settings;
  $links = [];
  if (isset($settings, $settings->modules))
    foreach ((array)$settings->modules as $name => $module) {
      if (isset($module->class) && ($module->active ?? TRUE))
        try {
          require_once("modules/{$name}.php");
          $links = array_merge($links, (new $module->class())->build_menu());
        } catch (Exception $e) {
          continue;
        }
    }

  $links = array_merge($links, (new BaseModule())->build_menu());
  echo "  <div id=\"menu\">\n".implode('', $links)."  </div>\n";
}

/******************************************************************************************
 * Build the styles link
 ******************************************************************************************/
function build_styles() {
  global $settings;
  if (isset($settings, $settings->style))
    foreach ($settings->style as $file)
      echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$file}\">\n";
}

/******************************************************************************************
 * Result class for queries
 ******************************************************************************************/
class QueryResult {
  private $res = NULL;

  function __construct($res) {
    $this->res = $res;
  }

  function __destruct() {
    if ($this->res)
      pg_free_result($this->res);
  }

  function fetch_all() {
    return $this->res ? pg_fetch_all($this->res) : FALSE;
  }

  function fetch_columns(int $column=0) {
    return $this->res ? pg_fetch_all_columns($this->res, $column) : FALSE;
  }

  function fetch_assoc(int $row=NULL) {
    return $this->res ? pg_fetch_assoc($this->res, $row) : FALSE;
  }

  function fetch_object(int $row=NULL) {
    return $this->res ? pg_fetch_object($this->res, $row) : FALSE;
  }

  function fetch(int $row=NULL) {
    return $this->res ? pg_fetch_row($this->res, $row) : FALSE;
  }
};

/******************************************************************************************
 * API Exception
 ******************************************************************************************/
class APIException extends Exception {
  public function __construct($message, array $endpoints=NULL, $code=0, Exception $previous=NULL) {
    parent::__construct($message, $code, $previous);
    $this->endpoints = $endpoints;
  }
};

/******************************************************************************************
 * Database wrapper class
 ******************************************************************************************/
class Database {
  // The actual database connection
  private $db = NULL;

  /****************************************************************************************
   * Construct the module instance and creates the tables if needed
   ****************************************************************************************/
  function __construct(string $host, string $database, string $user, string $password) {
    $this->db = pg_connect("host={$host} dbname={$database} user={$user} password={$password}");
  }

  /****************************************************************************************
   * Destructor which closes the database connection
   ****************************************************************************************/
  function __destruct() {
    if ($this->db)
      pg_flush($this->db);
  }

  /****************************************************************************************
   * Basic database query
   ****************************************************************************************/
  function query(string $query, array $params=NULL) {
    if (!$this->db)
      return FALSE;
    elseif ($params)
      return new \QueryResult(pg_query_params($this->db, $query, $params));
    else
      return new \QueryResult(pg_query($this->db, $query));
  }

  /****************************************************************************************
   * Create a new table using the column definitions
   ****************************************************************************************/
  function create_table(string $name, array $columns) {
    $cols = [];
    foreach ($columns as $cname => $cdef)
      $cols[] = $cname ? "\"{$cname}\" {$cdef}" : $cdef;

    $definitions = implode(',', $cols);
    $this->query("CREATE TABLE IF NOT EXISTS \"{$name}\" ({$definitions})");
  }

  /****************************************************************************************
   * Return if the table exists
   ****************************************************************************************/
  function exists_table(string $name) {
    $query = "SELECT 1 FROM information_schema.tables ".
      "WHERE table_schema = 'public' AND table_name = $1";

    return boolval($this->query($query, [$name])->fetch()[0]);
  }

  /****************************************************************************************
   * List all tables in the database
   ****************************************************************************************/
  function list_tables() {
    $query = "SELECT table_name FROM information_schema.tables ".
      "WHERE table_schema = 'public' AND table_type = 'BASE TABLE'";

    $result = $this->query($query);
    return $result->fetch_columns(0);
  }

  /****************************************************************************************
   * List all columns of the table
   ****************************************************************************************/
  function list_columns(string $table, array $columns=NULL) {
    $query = "SELECT column_name FROM information_schema.columns WHERE table_name = $1";
    $table_cols = $this->query($query, [$table])->fetch_columns(0);
    if (is_array($columns))
      return array_intersect($columns, $table_cols);
    return $table_cols;
  }

  /****************************************************************************************
   * List primary keys of the table
   ****************************************************************************************/
  function list_primary_keys(string $table) {
    $query = "SELECT column_name FROM information_schema.table_constraints AS tc ".
      "LEFT JOIN information_schema.constraint_column_usage AS ccu ".
      "ON ccu.constraint_name = tc.constraint_name ".
      "WHERE tc.table_name = $1 AND tc.constraint_type = 'PRIMARY KEY'";
    return $this->query($query, [$table])->fetch_columns(0);
  }

  /****************************************************************************************
   * Transaction function
   ****************************************************************************************/
  function begin() {$this->query("BEGIN TRANSACTION");}
  function commit() {$this->query("COMMIT TRANSACTION");}
  function end() {$this->query("END TRANSACTION");}
  function rollback() {$this->query("ROLLBACK");}

  /****************************************************************************************
   * Insert multiple items using the defined columns into the database
   *   Ignores conflicts and values without all columns
   ****************************************************************************************/
  function insert_many(string $table, array $data, array $cols=NULL, bool $replace=FALSE) {
    // Get the columns and primary keys of the table
    $columns = array_flip($this->list_columns($table, $cols));
    $primaries = [];
    foreach ($this->list_primary_keys($table) as $prim)
      $primaries[$prim] = "\"{$prim}\"";
    $prims = implode(',', $primaries);

    // Start the transaction
    $this->begin();
    foreach ($data as $row) {
      // Check if every primary key is given
      $skip = FALSE;
      foreach ($primaries as $primary => $val)
        if (!isset($row[$primary]))
          $skip = TRUE;

      if (!$skip) {
        $i = 1;
        // Build the query
        $sets = $values = $cells = $vars = [];
        foreach ($row as $cell => $value)
          if (isset($columns[$cell])) {
            $cells[] = "\"{$cell}\"";
            $values[] = $value;
            $vars[] = "\${$i}";
            if ($replace && !isset($primaries[$cell]))
              $sets[] = "\"{$cell}\" = \${$i}";
            $i++;
          }

        // Send the query
        if ($cells && $vars && $values) {
          $cells = implode(',', $cells);
          $vars = implode(',', $vars);
          $query = "INSERT INTO \"{$table}\" ({$cells}) VALUES ({$vars}) ";
          if ($replace && count($sets) > 0)
            $query .= "ON CONFLICT ({$prims}) DO UPDATE SET ".implode(', ', $sets);
          else
            $query .= "ON CONFLICT ({$prims}) DO NOTHING";
          $this->query($query, $values);
        }
      }
    }

    // Commit the transaction
    $this->end();
  }

  /****************************************************************************************
   * Insert a single row into the table
   ****************************************************************************************/
  function insert(string $table, array $row, array $cols=NULL, bool $replace=FALSE) {
    $this->insert_many($table, [$row], $cols, $replace);
  }

  /****************************************************************************************
   * Update multiple rows in the database used the defined primary keys
   ****************************************************************************************/
  function update_many(string $table, array $data, array $columns=NULL) {
    // Get the columns and primary keys of the table
    $columns = array_flip($this->list_columns($table, $columns));
    $primaries = $this->list_primary_keys($table);
    $primaries = array_combine($primaries, $primaries);

    // Start the transaction
    $this->begin();
    foreach ($data as $row) {
      // Check if every primary key is given
      $skip = FALSE;
      foreach ($primaries as $primary)
        if (!isset($row[$primary]))
          $skip = TRUE;

      if (!$skip) {
        $i = 1;
        $where = $set = $values = [];
        // Build the SET clause
        foreach ($row as $cell => $value)
          if (!isset($primaries[$cell])) {
            $set[] = "\"{$cell}\" = \${$i}";
            $values[] = $value;
            $i++;
          }

        // Build the WHERE clause
        foreach ($row as $cell => $value)
          if (isset($primaries[$cell])) {
            $where[] = "\"{$cell}\" = \${$i}";
            $values[] = $value;
            $i++;
          }

        // Build and send the query
        if ($set && $where && $values) {
          $set = implode(',', $set);
          $where = implode(' AND ', $where);
          $query = "UPDATE \"{$table}\" SET {$set} WHERE {$where}";
          $this->query($query, $values);
        }
      }
    }

    // Commit the transaction
    $this->end();
  }

  /****************************************************************************************
   * Update a single row of a table
   ****************************************************************************************/
  function update(string $table, array $row, array $columns=NULL) {
    $this->update_many($table, [$row], $columns);
  }
};

/******************************************************************************************
 * Base module which implements basic functions for all modules
 ******************************************************************************************/
class BaseModule extends Database {
  // Actions and endpoints
  public $action = [];
  // Configuration of the module
  private $config = NULL;
  // Global configuration
  private $global = NULL;
  // Menu entries of the module. Always visible if defined in BaseModule
  public $menu = [
    'update' => [
      'class' => 'fas fa-sync',
      'url' => 'update.php',
    ],
  ];
  // Name of the module
  public $name = '';
  // Tables of the modules
  protected $tables = [];
  // Version of the module
  public $version = '';
  // API settings
  private $api_token_length = 32;
  private $api_max_retries = 5;
  // Private table only for the base module configuration
  private $private_tables = [
    'modules' => [
      'name' => 'TEXT NOT NULL PRIMARY KEY',
      'timestamp' => 'TEXT NOT NULL',
      'version' => 'TEXT NOT NULL',
    ],
    'options' => [
      'module' => 'TEXT NOT NULL REFERENCES modules(name)',
      'name' => 'TEXT NOT NULL',
      'value' => 'TEXT',
      '' => 'PRIMARY KEY (module, name)',
    ],
    'last_access' => [
      'name' => 'TEXT NOT NULL PRIMARY KEY',
      'timestamp' => "TIMESTAMP WITHOUT TIME ZONE NOT NULL",
    ],
    'api_token' => [
      'id' => "SERIAL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
      'token' => "TEXT NOT NULL",
      'parent' => "INT",
      '' => "UNIQUE (token), UNIQUE(name, parent)",
    ],
    'api_right' => [
      'token_id' => "INT REFERENCES api_token(id)",
      'module' => "TEXT NOT NULL",
      'action' => "TEXT NOT NULL",
      '' => "PRIMARY KEY (token_id, module, action)",
    ],
  ];

  /****************************************************************************************
   * Construct the module instance and creates the tables if needed
   ****************************************************************************************/
  function __construct() {
    global $settings;
    $db = $settings->database;
    Database::__construct($db->host, $db->name, $db->user, $db->password);

    // Create the private module tables
    foreach ($this->private_tables as $table => $columns)
      $this->create_table($table, $columns);

    // Create the module tables
    foreach ($this->tables as $table => $columns)
      $this->create_table($table, $columns);

    $this->query(
      "INSERT INTO modules (\"name\", \"timestamp\", \"version\") ".
      "VALUES ('base', '', '') ON CONFLICT DO NOTHING");

    if (isset($settings->global))
      $this->global = (object)$settings->global;

    if (isset($this->global, $this->global->locale))
      setLocale(LC_ALL, $this->global->locale);

    // Insert the module name and version into the database
    if (strlen($this->name) && strlen($this->version)) {
      $this->check_version();

      // Use the configuration of the module
      if (isset($settings->modules, $settings->modules->{$this->name}))
        $this->config = (object)$settings->modules->{$this->name};

      $this->create_cache();
    }

    // Generate the root token if no token is stored
    if (!$this->get_root()) {
      $this->create_token("root");
      $token = $this->query("SELECT * FROM api_token LIMIT 1")->fetch_object();
    }

    if (isset($_SERVER, $_SERVER['DOCUMENT_URI'])) {
      $uri = $_SERVER['DOCUMENT_URI'];
      $now = new \DateTime();
      $query = 'INSERT INTO "last_access" ("name", "timestamp") VALUES ($1, $2) '.
        'ON CONFLICT ("name") DO UPDATE SET "timestamp" = $2';
      $this->query($query, [$uri, $now->format("Y-m-d H:i:s")]);
    }
  }

  /****************************************************************************************
   * Get the last access of a site
   ****************************************************************************************/
  function last_access($uri) {
    $res = $this->query('SELECT "timestamp" FROM "last_access" WHERE name = $1', [$uri]);
    if ($row = $res->fetch_object())
      return $row->timestamp;
    return NULL;
  }

  /****************************************************************************************
   * Call the value of the menu entry
   ****************************************************************************************/
  function menu_call($value) {
    if ($value && method_exists($this, $value))
      return $this->$value();
    return $value;
  }

  /****************************************************************************************
   * Builds the menu
   ****************************************************************************************/
  function build_menu() {
    $links = [];
    foreach ($this->menu as $name => $item) {
      $class = $this->menu_call($item['class'] ?? '');
      $symbol = $this->menu_call($item['symbol'] ?? '');
      $url = $item['url'] ?? '';
      $sub_icon = '';

      if ($value = $this->menu_call($item['sub-icon'] ?? NULL))
        $sub_icon = "<span class=\"sub-icon\">{$value}</span>";

      $links[] = "    <a href=\"{$url}\"><span class=\"{$class}\">{$symbol}{$sub_icon}</span></a>\n";
    }

    return $links;
  }

  /****************************************************************************************
   * Checks the version of a module or inserts it
   ****************************************************************************************/
  protected function check_version() {
    $query = 'SELECT version FROM "modules" WHERE name = $1';
    $row = $this->query($query, [$this->name])->fetch();

    if (!$row || $row[0] != $this->version)
      $this->insert('modules', [
        'name' => $this->name,
        'version' => $this->version,
        'timestamp' => gmdate('Y-m-d H:i:s'),
      ]);
  }

  /****************************************************************************************
   * Get the module specific configuration
   ****************************************************************************************/
  function config($key=NULL, $default=NULL) {
    if (is_null($key))
      return $this->config;
    if (!isset($this->config))
      return $default;
    else if (isset($key, $this->config->{$key}))
      return $this->config->{$key};
    else
      return $default;
  }

  /****************************************************************************************
   * Get the global configuration
   ****************************************************************************************/
  function global($key=NULL, $default=NULL) {
    if (is_null($key))
      return $this->global;
    if (!isset($this->global))
      return $default;
    else if (isset($key, $this->global->{$key}))
      return $this->global->{$key};
    else
      return $default;
  }

  /****************************************************************************************
   * Get the module configuration
   ****************************************************************************************/
  function modules($name=NULL, $default=NULL) {
    global $settings;
    if (is_null($name))
      return $settings->modules ?? NULL;
    if (!isset($setting->modules))
      return $default;
    else if (isset($name, $settings->modules->{$name}))
      return $settings->modules->{$name};
    else
      return $default;
  }

  /****************************************************************************************
   * List all modules
   ****************************************************************************************/
  function list_modules() {
    global $settings;
    if (isset($settings, $settings->modules))
      return array_keys((array)$settings->modules);
    return [];
  }

  /****************************************************************************************
   * Get the site last visited
   ****************************************************************************************/
  function get_last_site() {
    $query = "SELECT value FROM options WHERE module = 'base' AND name = 'last_access'";
    $res = $this->query($query);
    if ($row = $res->fetch())
      return $row[0];
    return NULL;
  }

  /****************************************************************************************
   * Set the last visited site
   ****************************************************************************************/
  function set_last_site($value) {
    if ($value) {
      $query = "INSERT INTO options (module, name, value) ".
        "VALUES ('base', 'last_access', $1) ON CONFLICT (module, name) ".
        "DO UPDATE SET value = $1";
      $this->query($query, [$value]);
    }
  }

  /****************************************************************************************
   * Get the option
   ****************************************************************************************/
  function get_option($name, $default=NULL) {
    $query = "SELECT value FROM options WHERE module = $1 AND name = $2";
    $res = $this->query($query, [$this->name, $name]);
    if ($row = $res->fetch())
      return $row[0];
    return $default;
  }

  /****************************************************************************************
   * Set the option
   ****************************************************************************************/
  function set_option($name, $value, $independent=FALSE) {
    $query = "INSERT INTO options (module, name, value) ".
      "VALUES ($1, $2, $3) ON CONFLICT (module, name) ".
      "DO UPDATE SET value = $3";
    $this->query($query, [$this->name, $name, $value]);
  }

  /****************************************************************************************
   * Create a cache directory
   ****************************************************************************************/
  protected function create_cache($dir=NULL) {
    $cache = $this->get_cache();
    if (!isset($cache))
      return FALSE;

    if (isset($dir))
      $cache .= trim($dir, " \t\n\r\0\x0B/.");

    if (is_dir($cache))
      return TRUE;

    $mask = umask(0);
    mkdir($cache, 0777, TRUE);
    umask($mask);
    return is_dir($dir);
  }

  /****************************************************************************************
   * Get the path to the cache
   ****************************************************************************************/
  protected function get_cache() {
    if (isset($this->config, $this->config->cache) && $this->config->cache) {
      $dir = "cache/{$this->name}/";
      if ($this->config->cache !== TRUE)
        $dir .= trim($this->config->cache, "\t\n\r\0\x0B/");
      return $dir;
    }
    return NULL;
  }

  /****************************************************************************************
   * Update the module
   ****************************************************************************************/
  function upgrade() {
    $this->update('modules', ['name' => $this->name, 'timestamp' => gmdate('Y-m-d H:i:s')]);
  }

  /****************************************************************************************
   * Generate a new token
   ****************************************************************************************/
  private function generate_token() {
    return bin2hex(random_bytes($this->api_token_length));
  }

  /****************************************************************************************
   * Main API method
   ****************************************************************************************/
  function api_action($token, string $action, array $params) {
    throw new Exception("Invalid endpoint");
  }

  /****************************************************************************************
   * Get the root token
   ****************************************************************************************/
  private function get_root() {
    $query = "SELECT * FROM api_token WHERE parent IS NULL";
    $res = $this->query($query);
    if ($row = $res->fetch_object())
      return $row;
    return NULL;
  }

  /****************************************************************************************
   * Get the information of a token
   ****************************************************************************************/
  private function get_token(string $token) {
    $query = "SELECT * FROM api_token WHERE token = $1";
    $res = $this->query($query, [$token]);
    if ($row = $res->fetch_object())
      return $row;
    return NULL;
  }

  /****************************************************************************************
   * Get the information of a token by id
   ****************************************************************************************/
  private function get_token_by_id(int $token_id) {
    $query = "SELECT * FROM api_token WHERE id = $1";
    $res = $this->query($query, [$token_id]);
    if ($row = $res->fetch_object())
      return $row;
    return NULL;
  }

  /****************************************************************************************
   * Check if a token is a child of another token
   ****************************************************************************************/
  private function is_child($token, $child) {
    if (isset($child, $child->parent) && $child->parent === $token->id)
      return TRUE;

    while ($child && $child->parent) {
      $child = $this->get_token_by_id($child->parent);

      if (is_null($child))
        return FALSE;

      if ($child->parent == $token->id)
        return TRUE;
    }
    return FALSE;
  }

  /****************************************************************************************
   * Get token info
   ****************************************************************************************/
  private function token_info($token) {
    $info = clone $token;
    $info->modules = $info->childs = [];

    if (is_null($token->parent)) {
      foreach($this->modules() as $name => $module) {
        try {
          require_once("modules/{$name}.php");
          $module = new $module->class();
          foreach ($module->action as $action)
            $info->modules[$name][] = $action;
        } catch (Exception $e) {}
      }
    } else {
      $query = 'SELECT * FROM api_right WHERE token_id = $1';
      $res = $this->query($query, [$token->id]);
      while ($row = $res->fetch_object()) {
        $module = $row->module;
        unset($row->module, $row->token_id);

        $info->modules[$module][] = $row->action;
      }
    }

    $query = 'SELECT * FROM api_token WHERE parent = $1';
    $res = $this->query($query, [$token->id]);
    while ($row = $res->fetch_object())
      $info->childs[] = (object)[
        'name' => $row->name,
        'token' => $row->token,
      ];

    unset($info->id, $info->parent);
    return $info;
  }

  /****************************************************************************************
   * Create new token and copy the rights
   ****************************************************************************************/
  private function create_token(string $name, int $parent=NULL) {
    if (is_null($parent) && $this->get_root())
      return NULL;

    // Try to create the token
    for ($i = 0; $i < $this->api_max_retries; $i++) {
      // The token must be unique
      $token = $this->generate_token();
      if (!$this->get_token($token)) {
        $data = ['name' => $name, 'token' => $token, 'parent' => NULL];
        $query = 'INSERT INTO api_token ("name", "token", "parent") VALUES ($1, $2, $3)';
        $this->query($query, [$name, $token, $parent]);

        // Get the token info
        $info = $this->get_token($token);
        unset($info->id, $info->parent);
        return $info;
      }
    }
    return NULL;
  }

  /****************************************************************************************
   * Delete a token
   ****************************************************************************************/
  private function delete_token($token) {
    $ids = [$token->id];
    $parms = [];
    for ($i = 0; $i < count($ids); $i++) {
      $id = $ids[$i];
      $parms[] = "\$".($i + 1);
      $res = $this->query("SELECT id FROM api_token WHERE parent = $1", [$id]);
      while ($row = $res->fetch_object())
        $ids[] = $row->id;
    }

    $parms = implode(",", $parms);
    $this->query("DELETE FROM api_right WHERE token_id IN ({$parms})", $ids);
    $this->query("DELETE FROM api_token WHERE id IN ({$parms})", $ids);
    return ["status" => "ok"];
  }

  /****************************************************************************************
   * Change rights of a token on a module action
   ****************************************************************************************/
  private function allow_action($token, $child, string $module, string $actions) {
    if (!$child || !$this->is_child($token, $child))
      throw new APIException("Invalid token");

    if ($actions === '*') {
      $actions = "";
      $modules = $this->modules();
      if (isset($modules->{$module})) {
        $mod = $modules->{$module};
        try {
          require_once("modules/{$module}.php");
          $mod = new $mod->class();
          $actions = implode(',', $mod->action);
        } catch (Exception $e) {}
      }
    }

    $ok = FALSE;
    $test = [];
    foreach (explode(',', $actions) as $action)
      if ($this->check_access($token, $module, $action)) {
        $test[] = $action;
        $query = 'INSERT INTO api_right (token_id, module, action) VALUES ($1, $2, $3)';
        $this->query($query, [$child->id, $module, $action]);
        $ok = TRUE;
      }

    if (!$ok)
      throw new APIException("Invalid token");
    return api_ok();
  }

  private function deny_action($token, $child, string $module, string $actions) {
    if (isset($child, $child->parent) && $child->parent == $token->id) {
      $query = 'DELETE FROM api_right '.
        'WHERE token_id = $1 AND module = $2 AND action = $3';

      foreach (explode(',', $actions) as $action)
        $this->query($query, [$child->id, $module, $action]);
    }

    return api_ok();
  }

  /****************************************************************************************
   * Check the access
   ****************************************************************************************/
  function check_access($token, string $module, string $action) {
    if (is_null($token->parent))
      return TRUE;

    $query = "SELECT 1 FROM api_right ".
      "WHERE token_id = $1 AND module = $2 AND action = $3";

    $res = $this->query($query, [$token->id, $module, $action]);
    if ($row = $res->fetch())
      return TRUE;
    return FALSE;
  }

  /****************************************************************************************
   * Pass the request to the modules
   ****************************************************************************************/
  private function module_api($token, string $path, array $params) {
    $modules = $this->modules();
    try {
      foreach($modules as $name => $module) {
        $pre = "/{$name}";
        if (startswith($path, "{$pre}/") || $path === $pre) {
          $action = ltrim(substr($path, strlen($pre)), '/');

          require_once("modules/{$name}.php");
          $module = new $module->class();

          if ($action == 'rights')
            return $module->action;
          return $module->api_action($token, $action, $params);
        }
      }
    } catch (APIException $e) {
      throw $e;
    } catch (Exception $e) {
      throw new APIException("Invalid endpoint");
    }
    throw new APIException("Invalid endpoint", array_keys((array)$modules));
  }

  /****************************************************************************************
   * Handle requests to the API
   ****************************************************************************************/
  function handle_request(string $token, array $params) {
    $token = $this->get_token($token);
    if (!$token)
      throw new APIException("Invalid token");

    $endpoints = array_keys((array)$this->modules());
    $endpoints[] = 'token';

    $path = $params['path'] ?? FALSE;
    if (!$path)
      throw new APIException("Invalid endpoint", $endpoints);

    $path = rtrim($path, '/');
    switch ($path) {
      // Get the token info
      case '/token': case '/token/info':
        return $this->token_info($token);

      // Create a new sub token
      case '/token/create':
        $name = $params['name'] ?? FALSE;
        if (!is_string($name))
          throw new APIException("Invalid name");

        if ($info = $this->create_token($name, $token->id))
          return $this->token_info($info);

        throw new APIException("Can't create token");

      // Allow the action of the module
      case '/token/allow':
        if (!isset($params['child']))
          throw new APIException("Missing child token");

        $child = $this->get_token($params['child']);
        if (!isset($child))
          throw new APIException("Invalid child token");

        else if (!isset($params['module']))
          throw new APIException("Missing parameter module");

        else if (!isset($params['action']))
          throw new APIException("Missing parameter action");

        return $this->allow_action($token, $child, $params['module'], $params['action']);

      // Deny the action of the module
      case '/token/deny':
        if (!isset($params['child']))
          throw new APIException("Missing child token");

        $child = $this->get_token($params['child']);
        if (!isset($child))
          throw new APIException("Invalid child token");

        else if (!isset($params['module']))
          throw new APIException("Missing parameter module");

        else if (!isset($params['action']))
          throw new APIException("Missing parameter action");

        return $this->deny_action($token, $child, $params['module'], $params['action']);

      // Delete the token
      case '/token/delete':
        return $this->delete_token($token);

      // Check for module API
      default:
        return $this->module_api($token, $path, $params);
    }

    throw new APIException("Invalid endpoint", $endpoints);
  }
};

/******************************************************************************************
 * Web API with options to download multiple files parallel
 ******************************************************************************************/
class WebAPI {
  // Default parallel downloader
  protected $max_parallel = 4;
  // Default header
  protected $header = [];
  // Username and password for HTTP Authentication
  public $username = FALSE;
  public $password = FALSE;

  /****************************************************************************************
   * Set a http header
   ****************************************************************************************/
  function set_header(string $key, string $value) {
    $this->header[$key] = $value;
  }

  /****************************************************************************************
   * Set the authorization
   ****************************************************************************************/
  function set_authorization(string $username, string $password) {
    $this->header['Authorization'] = 'Basic '.base64_encode($username.':'.$password);
  }

  /****************************************************************************************
   * Delete a http header
   ****************************************************************************************/
  function delete_header(string $key) {
    if (isset($this->header[$key]))
      unset($this->header[$key]);
  }

  /****************************************************************************************
   * Create the curl header
   ****************************************************************************************/
  protected function curl($url, string $method=NULL) {
    $header = [];
    foreach ($this->header as $key => $value)
      $header[] = "{$key}: {$value}";

    $ch = curl_init($url);
    curl_setopt_array($ch, [
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_SSL_VERIFYHOST => FALSE,
      CURLOPT_SSL_VERIFYPEER => FALSE,
      CURLOPT_HTTPHEADER => $header,
    ]);
    if ($this->username && $this->password)
      curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
    if ($method)
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    return $ch;
  }

  /****************************************************************************************
   * Do a single request
   ****************************************************************************************/
  function request(string $url, string $method=NULL) {
    $ch = $this->curl($url, $method);
    $headers = [];
    // curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HEADERFUNCTION,
      function($curl, $header) use (&$headers) {
        $len = strlen($header);
        $header = explode(':', $header, 2);
        if (count($header) < 2) // ignore invalid headers
          return $len;

        $headers[strtolower(trim($header[0]))][] = trim($header[1]);

        return $len;
      }
    );
    $response = curl_exec($ch);
    curl_close($ch);
    return (object)['header' => $headers, 'body' => $response];
  }

  /****************************************************************************************
   * Download a single URL
   ****************************************************************************************/
  function download(string $url, bool $json=FALSE, bool $assoc=FALSE) {
    $ch = $this->curl($url);
    $content = curl_exec($ch);
    if ($json)
      $content = json_decode($content, $assoc);
    curl_close($ch);
    return $content;
  }

  /****************************************************************************************
   * Download all URLs parallel
   ****************************************************************************************/
  function multiload(array $urls, bool $json=FALSE, bool $assoc=FALSE) {
    // Initialize the handlers
    $mh = curl_multi_init();
    $keys = array_keys($urls);
    $values = array_values($urls);
    $requests = [];
    for ($i = 0; $i < $this->max_parallel && $i < count($values); $i++) {
      $ch = $this->curl($values[$i]);
      $requests[(string)$ch] = $keys[$i];
      curl_multi_add_handle($mh, $ch);
    }

    // Execute the downloads and retrieve the data
    $data = [];
    $active = null;
    do {
      while (($execrun = curl_multi_exec($mh, $active)) == CURLM_CALL_MULTI_PERFORM);

      if ($execrun != CURLM_OK)
        break;

      // Procress readable data
      while ($done = curl_multi_info_read($mh)) {
        // Get the content
        $content = curl_multi_getcontent($done['handle']);
        if ($json)
          $content = json_decode($content, $assoc);

        $key = $requests[(string)$done['handle']];

        // Yield the content
        yield $key => $content;

        // Start a new request
        if ($i < sizeof($values) && isset($values[$i]) && $i < count($values)) {
          $ch = $this->curl($values[$i]);
          $requests[(string)$ch] = $keys[$i];
          curl_multi_add_handle($mh, $ch);
          $i++;
        }

        // Remove the old request
        curl_multi_remove_handle($mh, $done['handle']);
        curl_close($done['handle']);
      }
    } while ($active);

    // Close the handles
    curl_multi_close($mh);
  }
};
