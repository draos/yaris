<?php
require_once('base.php');

function sunrise($time, $city) {
  return date_sunrise($time, SUNFUNCS_RET_TIMESTAMP, $city->latitude, $city->longitude);
}

function sunset($time, $city) {
  return date_sunset($time, SUNFUNCS_RET_TIMESTAMP, $city->latitude, $city->longitude);
}

/******************************************************************************************
 * Wrapper class to handle the download and extraction of weather
 *   information using the openweathermap.org API
 ******************************************************************************************/
class OpenWeatherModule extends BaseModule {
  // Module name and version
  public $name = "openweathermap";
  public $version = "0.1";
  // Menu entries of the module
  public $menu = [
    'graph' => [
      'class' => 'fas fa-chart-area',
      'url' => 'graph.php',
    ],
    'map' => [
      'class' => 'fas fa-map',
      'url' => 'map.php',
    ],
    'weather' => [
      'class' => 'get_condition_of_today',
      // 'class' => 'fas fa-sun',
      'url' => 'weather.php',
    ],
  ];
  // The base url of the server
  protected $server = "https://api.openweathermap.org";
  // The api
  protected $api = NULL;
  // The api key
  public $api_key = '';
  // The city id
  public $city_id = '';
  // Override the tables
  protected $tables = [
    // City table
    'owm_city' => [
      'id' => 'INT PRIMARY KEY NOT NULL',
      'name' => 'TEXT NOT NULL',
      'latitude' => 'REAL NOT NULL',
      'longitude' => 'REAL NOT NULL',
      'country' => 'TEXT NOT NULL',
    ],
    // Table for the 3 hourly table
    'owm_hourly' => [
      'timestamp' => 'BIGINT NOT NULL',
      'time' => 'TEXT NOT NULL',
      'temp' => 'REAL NOT NULL',
      'temp_min' => 'REAL NOT NULL',
      'temp_max' => 'REAL NOT NULL',
      'wind_speed' => 'REAL NOT NULL',
      'wind_dir' => 'REAL NOT NULL',
      'humidity' => 'REAL NOT NULL',
      'pressure' => 'REAL NOT NULL',
      'pressure_sea' => 'REAL NOT NULL',
      'cloudiness' => 'REAL NOT NULL',
      'rain' => 'REAL NOT NULL',
      'snow' => 'REAL NOT NULL',
      'city_id' => 'INT NOT NULL REFERENCES owm_city(id)',
      '' => 'PRIMARY KEY (timestamp, city_id)',
    ],
    // Weather condition table
    'owm_condition' => [
      'id' => 'INT PRIMARY KEY NOT NULL',
      'main' => "TEXT DEFAULT '' NOT NULL",
      'desc' => "TEXT DEFAULT '' NOT NULL",
      'icon' => "TEXT DEFAULT '' NOT NULL",
    ],
    // Hourly to condition reference table
    'owm_hourly_condition' => [
      'timestamp' => 'BIGINT NOT NULL',
      'city_id' => 'INT NOT NULL',
      'condition_id' => 'INT NOT NULL REFERENCES owm_condition(id)',
      '' => 'PRIMARY KEY(timestamp, condition_id, city_id),'.
        'FOREIGN KEY(timestamp, city_id) REFERENCES owm_hourly(timestamp, city_id)',
    ],
  ];

  /****************************************************************************************
   * Used units of the weather data
   *   Standard:
   *     Temperature: Kelvin
   *     Wind speed: meter/second
   *   "metric":
   *     Temperature: Celsius
   *     Wind speed: meter/second
   *   "imperial":
   *     Temperature: Fahrenheit
   *     Wind speed: miles/hour
   ****************************************************************************************/
  public $units = "metric";

  /*************************************************************************************
   * Constructor which uses the settings to set the default api key and city id
   *************************************************************************************/
  function __construct() {
    parent::__construct();

    $this->api = new WebAPI();

    // Use the API key and city ID from the settings
    $this->api_key = $this->config('api_key');
    $this->city_id = $this->config('city_id');
  }

  /****************************************************************************************
   * Update the current data using the web api
   * Download the 5 day forecast data with data every 3 hours
   *   city
   *     city.id: City id
   *     city.name: City name
   *     city.coord
   *       city.coord.lat: Latitude
   *       city.coord.lon: Longitude
   *     city.country: Country code
   *   cnt
   *   list
   *     list[].dt
   *     list[].main
   *       list[].main.temp: Temperature
   *       list[].main.temp_min: Derived minimum temperature
   *       list[].main.temp_max: Derived maximum temperature
   *       list[].main.humidity: Humidity
   *       list[].main.pressure: Atmospheric pressure
   *       list[].main.sea_level: Atmospheric pressure on sea level
   *       list[].main.grnd_level: Atmospheric pressure on ground level
   *     list[].weather
   *       list[].weather[].id: Condition code
   *       list[].weather[].main: Condition group
   *       list[].weather[].description: Further condition description
   *       list[].weather[].icon: Condition icon
   *     list[].clouds
   *       list[].clouds.all: Cloudiness
   *     list[].wind
   *       list[].wind.speed: Wind speed
   *       list[].wind.deg: Wind direction
   *     list[].rain
   *       list[].rain.3h: Rain volume in 3 hours
   *     list[].snow
   *       list[].snow.3h: Snow volume in 3 hours
   ****************************************************************************************/
  function upgrade() {
    $url = "{$this->server}/data/2.5/forecast?".$this->arguments();
    $data = $this->api->download($url, TRUE);
    // Check using the city id
    if (!isset($data->city, $data->city->id) || $data->city->id !== $this->city_id)
      return FALSE;

    // Insert the city into the database
    $this->insert('owm_city', [
      'id' => $data->city->id,
      'name' => $data->city->name,
      'country' => $data->city->country,
      'longitude' => $data->city->coord->lon,
      'latitude' => $data->city->coord->lat,
    ]);

    // The database values
    $cs = $fs = $hcs = $ids = [];
    foreach ($data->list as $f) {
      $dt = new \DateTime('now');
      $dt->setTimestamp($f->dt);
      $ids[] = $f->dt;

      // Append the forecast data
      $fs[] = ['timestamp' => $f->dt, 'time' => $dt->format('Y-m-d H:i:s'),
        'temp' => $f->main->temp, 'temp_min' => $f->main->temp_min,
        'temp_max' => $f->main->temp_max, 'humidity' => $f->main->humidity,
        'pressure' => $f->main->pressure, 'pressure_sea' => $f->main->sea_level,
        'cloudiness' => $f->clouds->all, 'city_id' => $data->city->id,
        'rain' => isset($f->rain->{'3h'}) ? $f->rain->{'3h'} : 0,
        'snow' => isset($f->snow->{'3h'}) ? $f->snow->{'3h'} : 0,
        'wind_speed' => $f->wind->speed, 'wind_dir' => $f->wind->deg];

      // Append the weather codes
      foreach ($f->weather as $w) {
        $hcs[] = [
          'timestamp' => $f->dt,
          'condition_id' => $w->id,
          'city_id' => $data->city->id,
        ];

        $cs[$w->id] = [
          'id' => $w->id,
          'main' => $w->main,
          'desc' => $w->description,
          'icon' => $w->icon,
        ];
      }
    }

    // Insert the data into the database
    $this->insert_many('owm_hourly', $fs, NULL, TRUE);
    $this->insert_many('owm_condition', $cs, NULL, TRUE);
    $this->insert_many('owm_hourly_condition', $hcs, NULL, TRUE);

    parent::upgrade();
    return TRUE;
  }

  /****************************************************************************************
   * Build the HTTP get arguments
   ****************************************************************************************/
  protected function arguments() {
    return "id={$this->city_id}&APPID={$this->api_key}&units={$this->units}";
  }

  /****************************************************************************************
   * Convert the UTC Unix timestamp into a datetime object using the given timezone
   ****************************************************************************************/
  function convert_time($unix) {
    $dt = new \DateTime("now", (new \DateTimeZone("UTC")));
    $dt->setTimestamp($unix);
    if ($timezone = $this->global('timezone'))
      $dt->setTimezone(new \DateTimeZone($timezone));
    return $dt;
  }

  /****************************************************************************************
   * Get information about sunrise and sunset
   ****************************************************************************************/
  function sun($date, $city, $rise=FALSE) {
    $time = $date->getTimestamp();
    if ($rise)
      $time = sunrise($time, $city);
    else
      $time = sunset($time, $city);
    return $this->convert_time($time)->format('H:i');
  }

  /****************************************************************************************
   * Get information about the city
   ****************************************************************************************/
  function city() {
    $query = 'SELECT * FROM owm_city WHERE id = $1';
    $res = $this->query($query, [$this->city_id]);
    return $res->fetch_object();
  }

  /****************************************************************************************
   * Get information about a day
   ****************************************************************************************/
  function get_condition_of_today() {
    $query = "
      SELECT condition_id, COUNT(*) AS count
      FROM \"owm_hourly_condition\" AS c
      WHERE \"timestamp\" BETWEEN $1 AND $2
      GROUP BY condition_id ORDER BY count DESC LIMIT 1";

    $now = time();
    $day_end = (new \DateTime())->setTime(23, 59, 59)->getTimestamp();
    $res = $this->query($query, [$now, $day_end]);
    if ($row = $res->fetch()) {
      $city = $this->city();
      $sunrise = date_sunrise($now, SUNFUNCS_RET_TIMESTAMP, $city->latitude, $city->longitude);
      $sunset = date_sunset($now, SUNFUNCS_RET_TIMESTAMP, $city->latitude, $city->longitude);
      if ($sunrise <= $now && $now <= $sunset)
        return "wi wi-{$row[0]}";
      return "wi wi-night-{$row[0]}";
    }
    return 'fas fa-sun';
  }

  /****************************************************************************************
   * Get information about a day
   ****************************************************************************************/
  function daily($date) {
    $columns = implode(',', [
      'AVG(temp) AS temp', 'MIN(temp_min) AS temp_min', 'MAX(temp_max) AS temp_max',
      'AVG(pressure) AS pressure, AVG(pressure_sea) AS pressure_sea',
      'AVG(cloudiness) AS cloudiness', 'AVG(humidity) AS humidity',
      'AVG(wind_speed) as wind_speed', 'MAX(wind_speed) AS wind_speed_max',
      'SUM(rain) AS rain', 'SUM(snow) AS snow',
      '('.
        'SELECT c.id FROM "owm_condition" AS c ORDER BY ('.
          'SELECT COUNT(*) FROM "owm_hourly_condition" '.
          'WHERE condition_id = c.id AND timestamp IN ('.
            'SELECT timestamp FROM "owm_hourly" WHERE time like $1'.
          ')'.
        ') DESC LIMIT 1'.
      ') AS condition',
    ]);

    $date_s = $date->format('Y-m-d');
    $query = "SELECT {$columns} FROM \"owm_hourly\" WHERE \"time\" like $1";
    $res = $this->query($query, [$date_s.'%']);
    $city = $this->city();
    while ($row = $res->fetch_object()) {
      if (isset($row->temp)) {
        $data = $row;
        $data->sunrise = $this->sun($date, $city, TRUE);
        $data->sunset = $this->sun($date, $city, FALSE);
        $data->timestamp = $date_s;
        return $data;
      }
    }
    return FALSE;
  }

  /****************************************************************************************
   * Get the next hourly data
   ****************************************************************************************/
  function hourlies($start, $hours) {
    $columns = implode(',', [
      'temp', 'pressure', 'pressure_sea', 'cloudiness', 'humidity',
      'rain', 'snow', 'wind_dir', 'wind_speed', '(rain + snow) AS qpf',
    ]);

    $query = "SELECT {$columns} FROM \"owm_hourly\" ".
      "WHERE timestamp BETWEEN $1 AND $2 ORDER BY timestamp";
    $res = $this->query($query, [$start, $start + 3600 * $hours]);
    $data = [];
    while ($row = $res->fetch_object())
      $data[] = $row;
    return $data;
  }

  /****************************************************************************************
   * Get unit mapping
   ****************************************************************************************/
  function get_unit_mapping() {
    $mapping = [
      'temp' => 'K', 'pressure' => 'mbar', 'pressure_sea' => 'mbar',
      'cloudiness' => '%', 'humidity' => '%',
      'rain' => 'mm', 'snow' => 'mm', 'qpf' => 'mm',
      'wind_dir' => '°', 'wind_speed' => 'm/s',
    ];
    switch ($this->units) {
      case 'metric':
        $mapping['temp'] = '°C';
        break;
      case 'imperial':
        $mapping['temp'] = '°F';
        $mapping['wind_speed'] = 'mph';
        break;
    }
    return $mapping;
  }

  /****************************************************************************************
   * Get specific points of day (sunrise, sunset, noon, midnight)
   ****************************************************************************************/
  function get_specific_points($start, $hours) {
    $today = new \DateTime();
    $day = new \DateInterval("P1D");
    $city = $this->city();

    $points = [];
    for ($i = 0; $i <= $hours + 24; $i += 24) {
      $points[] = [(clone $today)->setTime(0, 0)->getTimestamp(), 'midnight'];
      $points[] = [sunrise($today->getTimestamp(), $city), 'sunrise'];
      $points[] = [(clone $today)->setTime(12, 0)->getTimestamp(), 'noon'];
      $points[] = [sunset($today->getTimestamp(), $city), 'sunset'];
      $today->add($day);
    }

    $result = [];
    $end = $start + 3600 * $hours;
    $span = 3600 * $hours;
    foreach ($points as $point)
      if ($start <= $point[0] && $point[0] <= $end)
        $result[] = [($point[0] - $start) / $span, $point[1]];
    return $result;
  }

  /***********************************************************************************************
   * Get the points for the graph
   ***********************************************************************************************/
  function get_points(int $start, int $hours) {
    $data = $this->hourlies($start, $hours);
    $count = count($data);
    if ($count < 2)
      return [];

    $bounds = [];
    foreach ($data as $point)
      foreach ($point as $key => $value) {
        $value = floatval($value);
        if (!isset($bounds[$key]))
          $bounds[$key] = [$value, $value];
        else {
          $bounds[$key][0] = min($bounds[$key][0], $value);
          $bounds[$key][1] = max($bounds[$key][1], $value);
        }
      }

    $ranges = [];
    foreach ($bounds as $key => $value)
      $ranges[$key] = $value[1] - $value[0];

    $result = [];
    foreach ($data as $i => $point) {
      $tmp = ['x' => $i / ($count - 1)];
      foreach ($point as $key => $value) {
        $range = $ranges[$key];
        $min = $bounds[$key][0];

        if ($range > 0.01)
          $tmp[$key] = 1.0 * ($value - $min) / $range;
        else
          $tmp[$key] = $value;
      }

      $result[] = $tmp;
    }
    return [$result, $bounds];
  }
};
