<?php
require_once('base.php');

/******************************************************************************************
 * Wrapper class to handle the download and extraction of xkcd comics
 ******************************************************************************************/
class XKCDModule extends BaseModule {
  // Module name and version
  public $name = "xkcd";
  public $version = "0.1";
  // Menu entries of the module
  public $menu = [
    'xkcd' => [
      'class' => 'fas fa-times',
      'url' => 'xkcd.php',
    ],
  ];
  // The api
  protected $api = NULL;
  // Override the tables
  protected $tables = [
    // Watchlist item table
    'xkcd_images' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'title' => "TEXT NOT NULL",
      'description' => "TEXT NOT NULL",
      'img' => "TEXT NOT NULL",
      'url' => "TEXT NOT NULL",
    ],
  ];

  /****************************************************************************************
   * Constructor which uses the settings
   ****************************************************************************************/
  function __construct() {
    parent::__construct();

    $this->api = new WebAPI();
  }

  /****************************************************************************************
   * Cache the item image
   ****************************************************************************************/
  protected function cache(&$item) {
    // Get the directory of the cache
    $cache = $this->get_cache();
    if (!isset($cache) || !is_dir($cache))
      return FALSE;

    if (strpos($item->img, $cache) !== 0) {
      // Get the file extension
      $ext = pathinfo($item->url, PATHINFO_EXTENSION);
      if (!$ext)
        return FALSE;

      // Build the path to the file and write the path back to the database
      $item->img = "{$cache}/{$item->id}.{$ext}";
      $this->update('xkcd_images', ['img' => $item->img, 'id' => $item->id]);
    }

    // Download the cached file if necessary
    if (is_file($item->img))
      return TRUE;

    // Download the file
    $data = file_get_contents($item->url);
    if ($data)
      file_put_contents($item->img, $data);
    return is_file($item->img);
  }

  /****************************************************************************************
   * Builds the icon cache
   ****************************************************************************************/
  function update_items($full=FALSE) {
    // Get the last number
    $url = "https://xkcd.com/info.0.json";
    $last = $this->api->download($url, TRUE);
    if (!isset($last, $last->num))
      return FALSE;

    // Get the stored ids
    $res = $this->query('SELECT id FROM "xkcd_images"');
    $ids = $res->fetch_columns();

    // Build the urls
    $urls = [];
    for ($i = 1; $i <= $last->num; $i++)
      if (!in_array($i, $ids))
        $urls[] = "https://xkcd.com/{$i}/info.0.json";

    // Collect the new items
    $data = [];
    foreach ($this->api->multiload($urls, TRUE) as $item)
      if (isset($item, $item->num, $item->safe_title, $item->alt, $item->img)) {
        $data[] = [
          'id' => $item->num,
          'title' => $item->safe_title,
          'description' => $item->alt,
          'img' => '',
          'url' => $item->img,
        ];
      }

    // Update the database
    if ($data) {
      $columns = ['id', 'title', 'description', 'img', 'url'];
      $this->insert_many('xkcd_images', $data, $columns);
    }

    return count($data);
  }

  /****************************************************************************************
   * Updates the database using the settings
   ****************************************************************************************/
  function upgrade($full=FALSE) {
    if ($this->update_items())
      parent::upgrade();
  }

  /****************************************************************************************
   * Returns the last valid id
   ****************************************************************************************/
  function last_id() {
    $res = $this->query('SELECT id FROM "xkcd_images" ORDER BY id DESC LIMIT 1');
    if ($row = $res->fetch())
      return $row[0];
    return FALSE;
  }

  /****************************************************************************************
   * Returns the specific item or the last one if the id is invalid
   ****************************************************************************************/
  function get($id=FALSE) {
    $order = (preg_match('/^\d+$/', $id)) ? '(id = $1) DESC,' : '';
    $query = "SELECT * FROM \"xkcd_images\" ORDER BY {$order} id DESC LIMIT 1";

    if ($order)
      $res = $this->query($query, [$id]);
    else
      $res = $this->query($query);

    // Get the item
    if ($item = $res->fetch_object()) {
      $item->cached = $this->cache($item);
      return $item;
    }
    return FALSE;
  }
};
