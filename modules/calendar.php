<?php

require_once('base.php');
require_once('caldav/SimpleCalDAVClient.php');

/******************************************************************************************
 * Wrapper class to handle the calendar
 ******************************************************************************************/
class CalendarModule extends BaseModule {
  // Module name and version
  public $name = "calendar";
  public $version = "0.1";
  // Menu entries of the module
  public $menu = [
    'calendar' => [
      'class' => 'fas fa-calendar-alt',
      'url' => 'calendar.php',
      'sub-icon' => 'get_number_of_today',
    ],
  ];

  /****************************************************************************************
   * Update the module
   ****************************************************************************************/
  function upgrade() {
    $this->count_today();
    parent::upgrade();
  }

  /****************************************************************************************
   * Get the number of events of today
   ****************************************************************************************/
  function get_number_of_today() {
    try {
      return intval($this->get_option('today', 0));
    } catch (Exception $e) {
      return parent::build_menu();
    }
  }

  /****************************************************************************************
   * Connect to the CalDAV server
   ****************************************************************************************/
  function connect() {
    $config = $this->config();
    if (!isset($config->url, $config->username, $config->password, $config->calendar))
      return FALSE;

    // Connect to the CalDAV server
    $client = new \SimpleCalDAVClient();
    try {
      $client->connect($config->url, $config->username, $config->password);
    } catch (Exception $e) {
      return FALSE;
    }

    // Get the calendar object and register it in the client
    $calendar = strtolower($config->calendar);
    foreach ($client->findCalendars() as $name => $cal)
      if ($name === $calendar) {
        $client->setCalendar($cal);
        return $client;
      }

    return FALSE;
  }

  /****************************************************************************************
   * Count the events of today
   ****************************************************************************************/
  function count_today() {
    $client = $this->connect();
    if (!$client)
      return FALSE;

    $start = new \DateTime();
    $start->setTime(0, 0);
    $end = clone $start;
    $end->add(new \DateInterval("P1D"));

    // Use the CalDAV time format
    $start = $start->format('Ymd\THis\Z');
    $end = $end->format('Ymd\THis\Z');

    // Search for events
    $counter = 0;
    foreach ($client->getEvents($start, $end) as $event)
      $counter++;

    // Set the number of events of today
    $this->set_option('today', $counter);
    return $counter;
  }

  /****************************************************************************************
   * Connects to the caldav server and get the events in the time span
   ****************************************************************************************/
  function get() {
    $client = $this->connect();

    if (!$client)
      return FALSE;

    // Calculate the timespan
    $days = $this->config("days", 31);

    $start = new \DateTime();
    $start->setTime(0, 0);
    $end = clone $start;
    $end->add(new \DateInterval("P{$days}D"));

    // Use the CalDAV time format
    $start = $start->format('Ymd\THis\Z');
    $end = $end->format('Ymd\THis\Z');

    // Search for events
    $events = [];
    $today = date('Ymd');
    $counter = 0;
    foreach ($client->getEvents($start, $end) as $event) {
      $data = $this->parse_ics($event->getData());

      if (!isset($data->vevent, $data->vevent[0]))
        continue;

      $vevent = $data->vevent[0];
      if (!isset($vevent->dtstart, $vevent->dtend, $vevent->summary))
        continue;

      if ($vevent->dtstart->format('Ymd') == $today)
        $counter++;

      $events[] = (object)[
        "title" => $vevent->summary,
        "start" => $vevent->dtstart,
        "end" => $vevent->dtend,
        "type" => $vevent->data_type ?? 'datetime',
      ];
    }

    $this->set_option('today', $counter);

    return $events;
  }

  /****************************************************************************************
   * Parse the calendar format and return an object
   ****************************************************************************************/
  function parse_ics($data) {
    $result = [];
    $type = NULL;
    $todos = $events = 0;

    foreach (explode("\n", $data) as $line) {
      if (!$line)
        continue;

      preg_match("/([^:]+)[:]([\w\W]*)/", $line, $matches);
      if (count($matches) == 0)
        continue;

      list($keyword, $value) = array_splice($matches, 1, 2);

      switch ($line) {
        case "BEGIN:VTODO":
          $type = "vtodo";
          $result["vtodo"][$todos++] = (object)[];
          break;
        case "BEGIN:VEVENT":
          $type = "vevent";
          $result["vevent"][$events++] = (object)[];
          break;
        case "BEGIN:VCALENDAR":
        case "BEGIN:DAYLIGHT":
        case "BEGIN:VTIMEZONE":
        case "BEGIN:STANDARD":
          $type = strtolower($value);
          break;
        case "END:VTODO":
        case "END:VEVENT":
        case "END:VCALENDAR":
        case "END:DAYLIGHT":
        case "END:VTIMEZONE":
        case "END:STANDARD":
          $type = "vcalendar";
          break;
        default:
          $data_type = FALSE;
          if (stristr($keyword, 'DTSTART') || stristr($keyword, "DTEND")) {
            $parts = explode(';', $keyword);
            $keyword = $parts[0];
            $value = new \DateTime($value);
            if (isset($parts[1]) && strpos($parts[1], "VALUE=") === 0)
              $data_type = strtolower(str_replace("VALUE=", "", $parts[1]));
          }

          $keyword = strtolower($keyword);

          switch ($type) {
            case "vtodo":
              $result[$type][$todos - 1]->{$keyword} = $value;
              if ($data_type)
                $result[$type][$todos - 1]->data_type = $data_type;
              break;
            case "vevent":
              $result[$type][$events - 1]->{$keyword} = $value;
              if ($data_type)
                $result[$type][$events - 1]->data_type = $data_type;
              break;
            default:
              if (!isset($result[$type]))
                $result[$type] = (object)[];

              $result[$type]->{$keyword} = $value;
              break;
          }
      }
    }

    return (object)$result;
  }
};
