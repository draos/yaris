<?php
require_once('base.php');

/******************************************************************************************
 * Wrapper class to handle the download and extraction of xkcd comics
 ******************************************************************************************/
class AnimeModule extends BaseModule {
  // Module name and version
  public $name = "anime";
  public $version = "0.1";
  // Menu entries of the module
  public $menu = [
    'anime' => [
      'class' => 'fas fa-film',
      'url' => 'anime.php',
      'sub-icon' => 'get_number_since_access',
    ],
  ];
  // Tables of the modules
  protected $tables = [
    'anime' => [
      'id' => 'SERIAL NOT NULL PRIMARY KEY',
      'name' => 'TEXT NOT NULL',
      'link' => 'TEXT NOT NULL',
      'sync_date' => "TIMESTAMP WITHOUT TIME ZONE DEFAULT '1970-01-01'",
      'create_date' => "TIMESTAMP WITHOUT TIME ZONE ".
        "DEFAULT (now() AT TIME ZONE 'utc')",
      '' => 'UNIQUE (name)',
    ],
    'season' => [
      'id' => 'SERIAL NOT NULL PRIMARY KEY',
      'anime_id' => 'INT NOT NULL REFERENCES anime(id)',
      'name' => 'TEXT NOT NULL',
      'link' => 'TEXT NOT NULL',
      'lang' => 'TEXT NOT NULL',
      'sync_date' => "TIMESTAMP WITHOUT TIME ZONE DEFAULT '1970-01-01'",
      'create_date' => "TIMESTAMP WITHOUT TIME ZONE ".
        "DEFAULT (now() AT TIME ZONE 'utc')",
      '' => 'UNIQUE (anime_id, name, lang)',
    ],
    'episode' => [
      'id' => 'SERIAL NOT NULL PRIMARY KEY',
      'anime_id' => 'INT NOT NULL REFERENCES anime(id)',
      'season_id' => 'INT NOT NULL REFERENCES season(id)',
      'name' => 'TEXT NOT NULL',
      'link' => 'TEXT NOT NULL',
      'create_date' => "TIMESTAMP WITHOUT TIME ZONE ".
        "DEFAULT (now() AT TIME ZONE 'utc')",
    ],
  ];

  /****************************************************************************************
   * Get the episode since the last access
   ****************************************************************************************/
  function get_number_since_access() {
    $last_access = $this->last_access('/anime.php');
    if (!$last_access)
      return NULL;

    $query = 'SELECT COUNT(*) AS c FROM "episode" WHERE "create_date" >= $1';
    $res = $this->query($query, [$last_access]);
    if ($row = $res->fetch_object())
      return $row->c;
    return NULL;
  }

  /****************************************************************************************
   * Update the module
   ****************************************************************************************/
  function upgrade() {}

  /****************************************************************************************
   * Returns the latest episodes
   ****************************************************************************************/
  function get_latest() {
    $maximum = $this->config('maximum', 10);
    $condition = [];

    $languages = $this->config('language', []);
    if (count($languages) > 0) {
      $condition[] = "('".implode("','", $languages)."')";
    }

    if (count($condition) > 0) {
      $condition = 'WHERE "s".lang IN '.implode(' AND ', $condition);
    } else
      $condition = "";

    $query = "SELECT \"a\".name AS anime, \"s\".lang, \"e\".name AS episode
      FROM \"episode\" AS \"e\"
      LEFT JOIN \"season\" as \"s\" ON \"s\".id = \"e\".season_id
      LEFT JOIN \"anime\" AS \"a\" ON \"a\".id = \"e\".anime_id
      {$condition}
      ORDER BY \"e\".\"create_date\" DESC, \"a\".name, \"e\".\"link\" DESC LIMIT $1";

    $data = [];
    $res = $this->query($query, [$maximum]);
    while ($row = $res->fetch_object())
      $data[] = $row;
    return $data;
  }

  /****************************************************************************************
   * Returns the animes
   ****************************************************************************************/
  function get_animes() {
    $query = 'SELECT "name" FROM "anime" ORDER BY name';
    $data = [];
    $res = $this->query($query);
    while ($row = $res->fetch_object())
      $data[] = $row->name;
    return $data;
  }
};
