<?php
require_once('base.php');

/******************************************************************************************
 * Wrapper class to handle the download and extraction of xkcd comics
 ******************************************************************************************/
class MangaModule extends BaseModule {
  // Module name and version
  public $name = "manga";
  public $version = "0.1";
  // Menu entries of the module
  public $menu = [
    'manga' => [
      'class' => 'fas fa-image',
      'url' => 'manga.php',
      'sub-icon' => 'get_number_since_access',
    ],
  ];
  // Tables of the modules
  protected $tables = [
    'manga' => [
      'id' => 'SERIAL NOT NULL PRIMARY KEY',
      'name' => 'TEXT NOT NULL UNIQUE',
      'link' => 'TEXT NOT NULL',
      'sync_date' => "TIMESTAMP WITHOUT TIME ZONE DEFAULT '1970-01-01'",
      'create_date' => "TIMESTAMP WITHOUT TIME ZONE ".
        "DEFAULT (now() AT TIME ZONE 'utc')",
    ],
    'chapter' => [
      'id' => "SERIAL NOT NULL PRIMARY KEY",
      'manga_id' => "INT NOT NULL REFERENCES manga(id)",
      'name' => "TEXT NOT NULL",
      'nr' => "TEXT NOT NULL",
      'link' => "TEXT NOT NULL",
      'sync_date' => "TIMESTAMP WITHOUT TIME ZONE DEFAULT '1970-01-01'",
      'create_date' => "TIMESTAMP WITHOUT TIME ZONE ".
        "DEFAULT (now() AT TIME ZONE 'utc')",
    ],
  ];

  /****************************************************************************************
   * Get the episode since the last access
   ****************************************************************************************/
  function get_number_since_access() {
    $last_access = $this->last_access('/manga.php');
    if (!$last_access)
      return NULL;

    $query = 'SELECT COUNT(*) AS c FROM "chapter" WHERE "create_date" >= $1';
    $res = $this->query($query, [$last_access]);
    if ($row = $res->fetch_object())
      return $row->c;
    return NULL;
  }

  /****************************************************************************************
   * Update the module
   ****************************************************************************************/
  function upgrade() {}

  /****************************************************************************************
   * Returns the latest chapters
   ****************************************************************************************/
  function get_latest() {
    $maximum = $this->config('maximum', 10);
    $condition = [];

    $query = "SELECT \"m\".name AS manga, \"c\".name AS chapter, \"c\".nr AS nr
      FROM \"chapter\" AS \"c\"
      LEFT JOIN \"manga\" as \"m\" ON \"m\".id = \"c\".manga_id
      ORDER BY \"c\".\"create_date\" DESC, \"c\".name, \"c\".\"nr\" DESC LIMIT $1";

    $data = [];
    $res = $this->query($query, [$maximum]);
    while ($row = $res->fetch_object())
      $data[] = $row;
    return $data;
  }

  /****************************************************************************************
   * Returns the mangas
   ****************************************************************************************/
  function get_mangas() {
    $query = 'SELECT "name" FROM "manga" ORDER BY name';
    $data = [];
    $res = $this->query($query);
    while ($row = $res->fetch_object())
      $data[] = $row->name;
    return $data;
  }
};
