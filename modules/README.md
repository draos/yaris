# BaseModule

### Configuration

* `$name`: Name of the module. String. Required.
* `$version`: Version of the module. String. Required.
* `$tables`: Definition of the database tables.
* `$menu`: Menu entries of the module.

### Functions

* update
* update_many
* insert
* insert_many
* begin
* end
* rollback
* list_primary_keys
* list_columns
* list_tables
* exists_table
* create_table
* query

* config
* global
* get_option
* set_option
* upgrade

# WebAPI

# Functions

* download
* multiload
* set_header