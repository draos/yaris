<?php
require_once('base.php');

function convert($coins) {
  $gold = floor($coins / 10000);
  $silver = floor($coins / 100) % 100;
  $copper = $coins % 100;
  $result = [];
  if ($gold)
    $result[] = "{$gold}<span class=\"gold\"></span>";
  if ($gold || $silver)
    $result[] = sprintf("%02s<span class=\"silver\"></span>", $silver);
  $result[] = sprintf("%02s<span class=\"copper\"></span>", $copper);
  return implode(' ', $result);
}

/******************************************************************************************
 * Wrapper class to access the Guild Wars 2 API
 ******************************************************************************************/
class Gw2API extends WebAPI {
  // The API key
  public $token;
  // The limit of a bulk download (200 is default from the API side)
  public $limit = 200;
  // The server to the API
  protected $server = 'https://api.guildwars2.com';
  // The used version of the API (v2 is recommended)
  protected $version;
  // Default language
  protected $language = 'en';

  /****************************************************************************************
   * Constructor which tackes the version and the API token
   ****************************************************************************************/
  function __construct($token='', $version='v2') {
    $this->token = $token;
    $this->version = $version;
  }

  /****************************************************************************************
   * Set the language in the header
   ****************************************************************************************/
  function set_language($language) {
    switch ($language) {
      case 'en': case 'de': case 'fr': case 'es':
        $this->language = $language;
        break;
    }

    $this->set_header('Accept-Language', $this->language);
  }

  /****************************************************************************************
   * Build the actual URL of the API request
   ****************************************************************************************/
  protected function build_url($endpoint, $params=[]) {
    $args = (count($params) > 0) ? '?'.http_build_query($params) : '';
    return "{$this->server}/{$this->version}/{$endpoint}{$args}";
  }

  /****************************************************************************************
   * Download a specific API site
   ****************************************************************************************/
  function apiload($endpoint, $access=FALSE) {
    $params = [];
    if ($access)
      $params['access_token'] = $this->token;

    $url = $this->build_url($endpoint, $params);
    return $this->download($url, TRUE);
  }

  /****************************************************************************************
   * Download all the items possible or needed in a bulk
   ****************************************************************************************/
  function bulkload($endpoint, $ids=FALSE, $access=FALSE) {
    // Download the ids if not given
    if (!is_array($ids))
      $ids = $this->apiload($endpoint, $access);

    // Build the parameters
    if ($access)
      $params['access_token'] = $this->token;

    // Generate the chunks
    $urls = [];
    foreach (array_chunk($ids, $this->limit) as $chunk) {
      $params['ids'] = implode(',', $chunk);
      $urls[] = $this->build_url($endpoint, $params);
    }

    foreach ($this->multiload($urls, TRUE) as $chunk)
      foreach ($chunk as $entry)
        yield $entry;
  }

  /****************************************************************************************
   * Synchronizing download which only loads needed items
   ****************************************************************************************/
  function syncload($endpoint, $ids, $access=FALSE, $sync=TRUE) {
    if ($sync) {
      $all = $this->apiload($endpoint, $access);
      return $this->bulkload($endpoint, array_diff($all, $ids), $access);
    }
    return $this->bulkload($endpo, $ids, $access);
  }

  /****************************************************************************************
   * API endpoints
   ****************************************************************************************/
  function account() {
    return $this->apiload('account', TRUE);
  }

  function account_achievements() {
    return $this->apiload('account/achievements', TRUE);
  }

  function account_bank() {
    return $this->apiload('account/bank', TRUE);
  }

  function account_dungeons() {
    return $this->apiload('account/dungeons', TRUE);
  }

  function account_dyes() {
    return $this->apiload('account/dyes', TRUE);
  }

  function account_finishers() {
    return $this->apiload('account/finishers', TRUE);
  }

  function account_gliders() {
    return $this->apiload('account/gliders', TRUE);
  }

  function account_home_cats() {
    return $this->apiload('account/home/cats', TRUE);
  }

  function account_home_nodes() {
    return $this->apiload('account/home/nodes', TRUE);
  }

  function account_inventory() {
    return $this->apiload('account/inventory', TRUE);
  }

  function account_mailcarriers() {
    return $this->apiload('account/mailcarriers', TRUE);
  }

  function account_masteries() {
    return $this->apiload('account/masteries', TRUE);
  }

  function account_mastery_points() {
    return $this->apiload('account/mastery/points', TRUE);
  }

  function account_materials() {
    return $this->apiload('account/materials', TRUE);
  }

  function account_minis() {
    return $this->apiload('account/minis', TRUE);
  }

  function account_outfits() {
    return $this->apiload('account/outfits', TRUE);
  }

  function account_pvp_heroes() {
    return $this->apiload('account/pvp/heroes', TRUE);
  }

  function account_raids() {
    return $this->apiload('account/raids', TRUE);
  }

  function account_recipes() {
    return $this->apiload('account/recipes', TRUE);
  }

  function account_skins() {
    return $this->apiload('account/skins', TRUE);
  }

  function account_titles() {
    return $this->apiload('account/titles', TRUE);
  }

  function account_wallet() {
    return $this->apiload('account/wallet', TRUE);
  }

  function achievements($ids=[], $sync=TRUE) {
    return $this->syncload('achievements', $ids, $sync);
  }

  function achievements_categories($ids=[], $sync=TRUE) {
    return $this->syncload('achievements/categories', $ids, $sync);
  }

  function achievements_daily($today=TRUE) {
    return $this->apiload('achievements/daily'.($today ? '' : '/tomorrow'));
  }

  function achievements_groups($ids=[], $sync=TRUE) {
    return $this->syncload('achievements/groups', $ids, $sync);
  }

  function backstory_answers($ids=[], $sync=TRUE) {
    return $this->syncload('backstory/answers', $ids, $sync);
  }

  function backstory_questions($ids=[], $sync=TRUE) {
    return $this->syncload('backstory/questions', $ids, $sync);
  }

  function build() {
    return $this->apiload('build');
  }

  function cats($ids=[], $sync=TRUE) {
    return $this->syncload('cats', $ids, $sync);
  }

  function characters($ids=[], $sync=TRUE) {
    return $this->syncload('characters', $ids, TRUE, $sync);
  }

  function characters_backstory($character_id) {
    return $this->apiload("characters/{$character_id}/backstory", TRUE);
  }

  function characters_core($character_id) {
    return $this->apiload("characters/{$character_id}/core", TRUE);
  }

  function characters_crafting($character_id) {
    return $this->apiload("characters/{$character_id}/crafting", TRUE);
  }

  function characters_equipment($character_id) {
    return $this->apiload("characters/{$character_id}/equipment", TRUE);
  }

  function characters_heropoints($character_id) {
    return $this->apiload("characters/{$character_id}/heropoints", TRUE);
  }

  function characters_inventory($character_id) {
    return $this->apiload("characters/{$character_id}/inventory", TRUE);
  }

  function characters_recipes($character_id) {
    return $this->apiload("characters/{$character_id}/recipes", TRUE);
  }

  function characters_sab($character_id) {
    return $this->apiload("characters/{$character_id}/sab", TRUE);
  }

  function characters_skills($character_id) {
    return $this->apiload("characters/{$character_id}/skills", TRUE);
  }

  function characters_spezializations($character_id) {
    return $this->apiload("characters/{$character_id}/spezializations", TRUE);
  }

  function characters_training($character_id) {
    return $this->apiload("characters/{$character_id}/training", TRUE);
  }

  function colors($ids=[], $sync=TRUE) {
    return $this->syncload('colors', $ids, $sync);
  }

  function commerce_delivery() {
    return $this->apiload('commerce/delivery', TRUE);
  }

  function commerce_exchange_coins() {
    return $this->apiload('commerce/exchange/coins');
  }

  function commerce_exchange_gems() {
    return $this->apiload('commerce/exchange/gems');
  }

  function commerce_listings($ids) {
    return $this->bulkload('commerce/listings', $ids);
  }

  function commerce_prices($ids) {
    return $this->bulkload('commerce/prices', $ids);
  }

  function commerce_transactions() {
    return $this->apiload('commerce/transactions', TRUE);
  }

  function continents($ids=[], $sync=TRUE) {
    return $this->syncload('continents', $ids, $sync);
  }

  function continents_floors($continent, $ids=[], $sync=TRUE) {
    return $this->syncload("continents/{$continent}/floors", $ids, $sync);
  }

  function continent_regions($continent, $floor, $ids=[], $sync=TRUE) {
    return $this->syncload(
      "continents/{$continent}/floors/{$floor}/regions", $ids, $sync
    );
  }

  function continent_maps($continent, $floor, $region, $ids=[], $sync=TRUE) {
    return $this->syncload(
      "continents/{$continent}/floors/{$floor}/regions/{$region}/maps", $ids, $sync
    );
  }

  function continent_pois($continent, $floor, $region, $map) {
    return $this->apiload(
      "continents/{$continent}/floors/{$floor}/regions/{$region}/maps/{$map}/pois"
    );
  }

  function continent_sectors($continent, $floor, $region, $map) {
    return $this->apiload(
      "continents/{$continent}/floors/{$floor}/regions/{$region}/maps/{$map}/sectors"
    );
  }

  function continent_tasks($continent, $floor, $region, $map) {
    return $this->apiload(
      "continents/{$continent}/floors/{$floor}/regions/{$region}/maps/{$map}/tasks"
    );
  }

  function currencies($ids=[], $sync=TRUE) {
    return $this->syncload('currencies', $ids, $sync);
  }

  function dungeons($ids=[], $sync=TRUE) {
    return $this->syncload('dungeons', $ids, $sync);
  }

  function emblem_backgrounds($ids=[], $sync=TRUE) {
    return $this->syncload('emblem/backgrounds', $ids, $sync);
  }

  function emblem_foregrounds($ids=[], $sync=TRUE) {
    return $this->syncload('emblem/foregrounds', $ids, $sync);
  }

  function files($ids=[], $sync=TRUE) {
    return $this->syncload('files', $ids, $sync);
  }

  function finishers($ids=[], $sync=TRUE) {
    return $this->syncload('finishers', $ids, $sync);
  }

  function gliders($ids=[], $sync=TRUE) {
    return $this->syncload('gliders', $ids, $sync);
  }

  function guild($guild_id) {
    return $this->apiload("guild/{$guild_id}");
  }

  function guild_log($guild_id) {
    return $this->apiload("guild/{$guild_id}/log", TRUE);
  }

  function guild_members($guild_id) {
    return $this->apiload("guild/{$guild_id}/members", TRUE);
  }

  function guild_ranks($guild_id) {
    return $this->apiload("guild/{$guild_id}/ranks", TRUE);
  }

  function guild_stash($guild_id) {
    return $this->apiload("guild/{$guild_id}/stash", TRUE);
  }

  function guild_teams($guild_id) {
    return $this->apiload("guild/{$guild_id}/teams", TRUE);
  }

  function guild_treasury($guild_id) {
    return $this->apiload("guild/{$guild_id}/treasury", TRUE);
  }

  function guild_upgrades_of($guild_id) {
    return $this->apiload("guild/{$guild_id}/upgrades", TRUE);
  }

  function guild_permissions($ids=[], $sync=TRUE) {
    return $this->syncload('guild/permissions', $ids, $sync);
  }

  function guild_search($guild_name) {
    return $this->apiload("guild/search?name={$guild_name}");
  }

  function guild_upgrades($ids=[], $sync=TRUE) {
    return $this->syncload('guild/upgrades', $ids, $sync);
  }

  function items($ids=[], $sync=TRUE) {
    return $this->syncload('items', $ids, $sync);
  }

  function itemstats($ids=[], $sync=TRUE) {
    return $this->syncload('itemstats', $ids, $sync);
  }

  function legends($ids=[], $sync=TRUE) {
    return $this->syncload('legends', $ids, $sync);
  }

  function mailcarriers($ids=[], $sync=TRUE) {
    return $this->syncload('mailcarriers', $ids, $sync);
  }

  function maps($ids=[], $sync=TRUE) {
    return $this->syncload('maps', $ids, $sync);
  }

  function masteries($ids=[], $sync=TRUE) {
    return $this->syncload('masteries', $ids, $sync);
  }

  function materiels($ids=[], $sync=TRUE) {
    return $this->syncload('materiels', $ids, $sync);
  }

  function minis($ids=[], $sync=TRUE) {
    return $this->syncload('minis', $ids, $sync);
  }

  function nodes($ids=[], $sync=TRUE) {
    return $this->syncload('nodes', $ids, $sync);
  }

  function outfits($ids=[], $sync=TRUE) {
    return $this->syncload('outfits', $ids, $sync);
  }

  function pets($ids=[], $sync=TRUE) {
    return $this->syncload('pets', $ids, $sync);
  }

  function professions($ids=[], $sync=TRUE) {
    return $this->syncload('professions', $ids, $sync);
  }

  function pvp_amulets($ids=[], $sync=TRUE) {
    return $this->syncload('pvp/amulets', $ids, $sync);
  }

  function pvp_games($ids=[], $sync=TRUE) {
    return $this->syncload('pvp/games', $ids, TRUE, $sync);
  }

  function pvp_heroes($ids=[], $sync=TRUE) {
    return $this->syncload('pvp/heroes', $ids, $sync);
  }

  function pvp_ranks($ids=[], $sync=TRUE) {
    return $this->syncload('pvp/ranks', $ids, $sync);
  }

  function pvp_seasons($ids=[], $sync=TRUE) {
    return $this->syncload('pvp/seasons', $ids, $sync);
  }

  function pvp_standings() {
    return $this->apiload('pvp/standings', TRUE);
  }

  function pvp_stats() {
    return $this->apiload('pvp/stats', TRUE);
  }

  function quaggans($ids=[], $sync=TRUE) {
    return $this->syncload('quaggans', $ids, $sync);
  }

  function races($ids=[], $sync=TRUE) {
    return $this->syncload('races', $ids, $sync);
  }

  function raids() {
    return $this->bulkload('raids');
  }

  function recipes($ids=[], $sync=TRUE) {
    return $this->syncload('recipes', $ids, $sync);
  }

  function recipes_input($item_id) {
    return $this->apiload('recipes/search?input={$item_id}');
  }

  function recipes_output($ítem_id) {
    return $this->apiload('recipes/search?output={$item_id}');
  }

  function skins($ids=[], $sync=TRUE) {
    return $this->syncload('skins', $ids, $sync);
  }

  function skills($ids=[], $sync=TRUE) {
    return $this->syncload('skills', $ids, $sync);
  }

  function spezializations($ids=[], $sync=TRUE) {
    return $this->syncload('spezializations', $ids, $sync);
  }

  function stories($ids=[], $sync=TRUE) {
    return $this->syncload('stories', $ids, $sync);
  }

  function stories_seasons($ids=[], $sync=TRUE) {
    return $this->syncload('stories/seasons', $ids, $sync);
  }

  function titles($ids=[], $sync=TRUE) {
    return $this->syncload('titles', $ids, $sync);
  }

  function tokeninfo() {
    return $this->apiload('tokeninfo', TRUE);
  }

  function traits($ids=[], $sync=TRUE) {
    return $this->syncload('traits', $ids, $sync);
  }

  function worlds($ids=[], $sync=TRUE) {
    return $this->syncload('worlds', $ids, $sync);
  }

  function wvw_abilities($ids=[], $sync=TRUE) {
    return $this->syncload('wvw/abilities', $ids, $sync);
  }

  function wvw_match($world_id) {
    return $this->apiload("wvw/matches?world={$world_id}");
  }

  function wvw_matches($ids=[], $sync=TRUE) {
    return $this->syncload('wvw/matches', $ids, $sync);
  }

  function wvw_objectives($ids=[], $sync=TRUE) {
    return $this->syncload('wvw/objectives', $ids, $sync);
  }

  function wvw_ranks($ids=[], $sync=TRUE) {
    return $this->syncload('wvw/ranks', $ids, $sync);
  }

  function wvw_upgrades($ids=[], $sync=TRUE) {
    return $this->syncload('wvw/upgrades', $ids, $sync);
  }
};


/******************************************************************************************
 * Wrapper class to handle the download and extraction of Guild Wars 2
 *   information using the API
 ******************************************************************************************/
class Gw2Module extends BaseModule {
  // Module name and version
  public $name = "guildwars2";
  public $version = "0.1";
  // Menu entries of the module
  public $menu = [
    'watchlist' => [
      'class' => 'fas fa-list',
      'url' => 'watchlist.php',
    ],
  ];
  // The api
  protected $api = NULL;
  // Override the tables
  protected $tables = [
    // Watchlist item table
    'gw2_continents' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL UNIQUE",
      'min_zoom' => "INT NOT NULL DEFAULT 0",
      'max_zoom' => "INT NOT NULL",
      'width' => "INT NOT NULL",
      'height' => "INT NOT NULL",
    ],
    'gw2_items' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
      'icon' => "TEXT NOT NULL",
      'url' => "TEXT NOT NULL",
      'buy_price' => "INT DEFAULT 0",
      'sell_price' => "INT DEFAULT 0",
      'timestamp' => "TEXT NOT NULL",
    ],
    'gw2_item_skins' => [
      'item_id' => "INT NOT NULL REFERENCES gw2_items(id)",
      'skin_id' => "INT NOT NULL REFERENCES gw2_skins(id)",
      '' => "PRIMARY KEY(item_id, skin_id)",
    ],
    'gw2_skins' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
      'blc_id' => "INT REFERENCES gw2_blc(id)",
    ],
    'gw2_blc' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
    ],
    'gw2_worlds' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
      'population' => "TEXT DEFAULT ''",
      'color' => "INT DEFAULT 0",
      'tier' => "TEXT DEFAULT ''",
    ],
    'gw2_maps' => [
      'id' => "INT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
      'continent_id' => "INT NOT NULL REFERENCES gw2_continents(id)",
      'cleft' => "real",
      'ctop' => "real",
      'cright' => "real",
      'cbottom' => "real",
      'mleft' => "real",
      'mtop' => "real",
      'mright' => "real",
      'mbottom' => "real",
    ],
    'gw2_objectives' => [
      'id' => "TEXT NOT NULL PRIMARY KEY",
      'name' => "TEXT NOT NULL",
      'type' => "TEXT",
      'map_id' => "INT NOT NULL REFERENCES gw2_maps(id)",
      'x' => "real",
      'y' => "real",
      'z' => "real",
      'marker' => "TEXT",
    ],
    'gw2_matches' => [
      'tier' => "TEXT NOT NULL",
      'color' => "INT NOT NULL",
      'score' => "INT DEFAULT 0",
      'deaths' => "INT DEFAULT 0",
      'kills' => "INT DEFAULT 0",
      'victories' => "INT DEFAULT 0",
      '' => "PRIMARY KEY(tier, color)",
    ],
    'gw2_match_maps' => [
      'timestamp' => "TEXT NOT NULL",
      'tier' => "TEXT NOT NULL",
      'color' => "INT NOT NULL",
      'map' => "INT NOT NULL",
      'score' => "INT DEFAULT 0",
      'deaths' => "INT DEFAULT 0",
      'kills' => "INT DEFAULT 0",
      '' => "PRIMARY KEY(timestamp, tier, color, map)",
    ],
  ];

  /****************************************************************************************
   * Constructor which uses the settings
   ****************************************************************************************/
  function __construct() {
    parent::__construct();
    $this->api = new \Gw2API();
    $this->api->set_language($this->global('language', 'en'));

    if (!$this->get_continents())
      $this->upgrade_continents(TRUE);
    if (!$this->get_maps())
      $this->upgrade_maps(TRUE);
    if (!$this->get_objectives())
      $this->upgrade_objectives(TRUE);
  }

  /****************************************************************************************
   * Map colors to integers
   ****************************************************************************************/
  function color_to_int($color) {
    switch ($color) {
      case 'green': return 1;
      case 'blue': return 2;
      case 'red': return 3;
    }
    return 0;
  }

  /****************************************************************************************
   * Map ints to colors
   ****************************************************************************************/
  function int_to_color($color) {
    switch ($color) {
      case 1: return 'green';
      case 2: return 'blue';
      case 3: return 'red';
    }
    return '';
  }

  /****************************************************************************************
   * Map to integer
   ****************************************************************************************/
  function map_to_int($map) {
    switch ($map) {
      case 'GreenHome': return 1;
      case 'BlueHome': return 2;
      case 'RedHome': return 3;
      case 'Center': return 4;
    }
    return 0;
  }

  /****************************************************************************************
   * Integer to map
   ****************************************************************************************/
  function int_to_map($map) {
    switch ($map) {
      case 1: return 'green';
      case 2: return 'blue';
      case 3: return 'red';
      case 4: return 'center';
    }
    return '';
  }

  /****************************************************************************************
   * Updates the database using the settings
   ****************************************************************************************/
  function upgrade() {
    $lang = $this->global('language', 'en');
    $full = $this->get_option('language') != $lang;

    $this->upgrade_items($full);
    $this->upgrade_blc($full);
    $this->upgrade_worlds($full);
    // Update the timestamp if an update happend
    if ($this->upgrade_prices())
      parent::upgrade();

    $this->set_option('language', $lang);
  }

  /****************************************************************************************
   * Updates the continents in the database
   ****************************************************************************************/
  protected function upgrade_continents($force=FALSE) {
    if (idate('H') != 0 && !$force)
      return 0;

    // Download the item prices
    $data = [];
    foreach ($this->api->continents() as $item)
      if (isset($item, $item->id, $item->name))
        $data[] = [
          "id" => $item->id,
          "name" => $item->name,
          "min_zoom" => $item->min_zoom,
          "max_zoom" => $item->max_zoom,
          "width" => $item->continent_dims[0],
          "height" => $item->continent_dims[1],
        ];

    // Update the continents in the database
    $this->insert_many('gw2_continents', $data, NULL, TRUE);

    return count($data);
  }

  /****************************************************************************************
   * Updates the maps in the database
   ****************************************************************************************/
  protected function upgrade_maps($force=FALSE) {
    if (idate('H') != 0 && !$force)
      return 0;

    // Download the item prices
    $data = [];
    foreach ($this->api->maps() as $item)
      if (isset($item, $item->id, $item->name, $item->continent_id))
        $data[] = [
          "id" => $item->id,
          "name" => $item->name,
          "continent_id" => $item->continent_id,
          "cleft" => $item->continent_rect[0][0],
          "ctop" => $item->continent_rect[0][1],
          "cright" => $item->continent_rect[1][0],
          "cbottom" => $item->continent_rect[1][1],
          "mleft" => $item->map_rect[0][0],
          "mtop" => $item->map_rect[0][1],
          "mright" => $item->map_rect[1][0],
          "mbottom" => $item->map_rect[1][1],
        ];

    // Update the maps in the database
    $this->insert_many('gw2_maps', $data, NULL, TRUE);

    return count($data);
  }

  /****************************************************************************************
   * Updates the objectives in the database
   ****************************************************************************************/
  protected function upgrade_objectives($force=FALSE) {
    if (idate('H') != 0 && !$force)
      return 0;

    // Download the item prices
    $data = [];
    foreach ($this->api->wvw_objectives() as $item)
      if (isset($item, $item->id, $item->name, $item->coord))
        $data[] = [
          "id" => $item->id,
          "name" => $item->name,
          "x" => $item->coord[0],
          "y" => $item->coord[1],
          "z" => $item->coord[2],
          "marker" => $item->marker,
          "map_id" => $item->map_id,
          "type" => $item->type,
        ];

    // Update the continents in the database
    $this->insert_many('gw2_objectives', $data, NULL, TRUE);

    return count($data);
  }

  /****************************************************************************************
   * Builds the icon cache
   ****************************************************************************************/
  protected function upgrade_icons($full=FALSE) {
    $cache = $this->get_cache();
    if (!isset($cache) || !is_dir($cache))
      return FALSE;

    $i = 1;
    $ids = $this->get_item_ids();
    $vals = [];
    foreach ($ids as $id) {
      $vals[] = "\${$i}";
      $i++;
    }

    $vals = implode(',', $vals);
    $query = "SELECT id, url FROM \"gw2_items\" WHERE id IN ({$vals})";
    $res = $this->query($query, $ids);

    // Check which icon isn't cached
    $icons = $items = [];
    while ($row = $res->fetch_object()) {
      $ext = pathinfo($row->url, PATHINFO_EXTENSION);
      $filename = "{$cache}{$row->id}.{$ext}";
      // Check if the file exists or if full upgrade
      if ($ext && (!is_file($filename) || $full)) {
        $icons[$filename] = $row->url;
        $items[] = ['id' => $row->id, 'icon' => '/'.$filename];
      }
    }

    // Download the icons and store them
    foreach ($this->api->multiload($icons) as $filename => $data)
      file_put_contents($filename, $data);

    // Write the cached icon paths back into the database
    if ($items)
      $this->update_many('gw2_items', $items);

    return count($icons);
  }

  /****************************************************************************************
   * Updates the item informations such as name and icon
   ****************************************************************************************/
  protected function upgrade_skins($full=FALSE) {
    // Don't download the cached items again if not forced to
    $cached = [];
    if (!$full) {
      $res = $this->query('SELECT id FROM "gw2_skins"');
      while ($row = $res->fetch())
        $cached[] = $row[0];
    }

    $skins = [];
    foreach ($this->api->skins($cached) as $skin)
      if (isset($skin, $skin->id, $skin->name))
        $skins[] = ['id' => $skin->id, 'name' => $skin->name];

    $this->insert_many('gw2_skins', $skins, NULL, TRUE);
  }

  /****************************************************************************************
   * Updates the item informations such as name and icon
   ****************************************************************************************/
  protected function upgrade_items($full=FALSE) {
    $this->upgrade_skins();

    // Don't download the cached items again if not forced to
    $cached = [];
    if (!$full) {
      $res = $this->query('SELECT id FROM "gw2_items"');
      while ($row = $res->fetch())
        $cached[] = $row[0];
    }

    // Get the currently stored skins
    $res = $this->query('SELECT id FROM "gw2_skins"');
    $current = array_flip($res->fetch_columns());

    // Download the item informations
    $items = $icons = $maps = $skins = [];
    $now = gmdate('Y-m-d H:i:s');
    foreach ($this->api->items($cached) as $item)
      if (isset($item, $item->name, $item->id, $item->icon)) {
        $icons[$item->id] = $item->icon;
        $items[] = [
          'id' => $item->id,
          'name' => $item->name,
          'icon' => $item->icon,
          'url' => $item->icon,
          'timestamp' => $now,
          'buy_price' => 0,
          'sell_price' => 0,
        ];

        if (isset($item->default_skin)) {
          if (!isset($current[$item->default_skin]))
            $skins[] = ['id' => $item->default_skin, 'name' => $item->name];

          $maps[] = ['skin_id' => $item->default_skin, 'item_id' => $item->id];
        }

        if (isset($item->details, $item->details->skins)) {
          foreach ($item->details->skins as $skin) {
            if (!isset($current[$skin]))
              $skins[] = ['id' => $skin, 'name' => $item->name];

            $maps[] = ['skin_id' => $skin, 'item_id' => $item->id];
          }
        }
      }

    // Insert the items into the table
    $this->insert_many('gw2_items', $items, NULL, TRUE);
    $this->insert_many('gw2_skins', $skins);
    $this->insert_many('gw2_item_skins', $maps);

    $this->upgrade_icons($full);

    return count($items);
  }

  /****************************************************************************************
   * Updates the prices of all items stored in the database
   ****************************************************************************************/
  protected function upgrade_prices() {
    // Get the item ids from the database
    $ids = $this->get_item_ids();

    // Add the item ids from the black lion collections
    $query = 'SELECT its.item_id FROM "gw2_skins" AS s '.
      'LEFT JOIN "gw2_item_skins" AS its ON its.skin_id = s.id '.
      'WHERE s.blc_id IS NOT NULL';
    $ids = array_merge($ids, $this->query($query)->fetch_columns());

    // Check if there are items stored
    if (count($ids) == 0)
      return FALSE;

    // Download the item prices
    $data = [];
    foreach ($this->api->commerce_prices($ids) as $item)
      if (isset($item, $item->id, $item->buys, $item->sells))
        $data[] = [
          'id' => $item->id,
          'buy_price' => $item->buys->unit_price,
          'sell_price' => $item->sells->unit_price,
        ];

    // Update the prices in the database
    $this->update_many('gw2_items', $data);

    return count($data);
  }

  /****************************************************************************************
   * Update the world data
   ****************************************************************************************/
  protected function upgrade_worlds($full=FALSE) {
    // Download the world data
    $worlds = [];
    foreach ($this->api->worlds() as $world)
      if (isset($world, $world->id, $world->name, $world->population))
        $worlds[$world->id] = (array)$world;

    // Get the current data
    $current = [];
    $res = $this->query("SELECT id, tier FROM gw2_worlds");
    while ($row = $res->fetch_object())
      $current[$row->id] = $row->tier;

    // Update the match data
    $matches = [];
    $maps = [];
    $na = $eu = FALSE;
    $colors = ['green', 'blue', 'red'];
    $now = gmdate('Y-m-d H:i:s');
    foreach ($this->api->wvw_matches() as $match) {
      // Append the match information
      foreach ($colors as $color) {
        try {
          $matches[] = [
            'tier' => $match->id,
            'color' => $this->color_to_int($color),
            'score' => $match->scores->{$color},
            'deaths' => $match->deaths->{$color},
            'kills' => $match->kills->{$color},
            'victories' => $match->victory_points->{$color},
          ];
        } catch (Exception $e) {
          continue;
        }

        // Append the map information
        try {
          foreach ($match->maps as $map)
            $maps[] = [
              'timestamp' => $now,
              'tier' => $match->id,
              'map' => $this->map_to_int($map->type),
              'color' => $this->color_to_int($color),
              'score' => $map->scores->{$color},
              'deaths' => $map->deaths->{$color},
              'kills' => $map->kills->{$color},
            ];
        } catch (Exception $e) {
          continue;
        }
      }

      // Append the match information
      if (isset($match, $match->id, $match->all_worlds)) {
        foreach ($match->all_worlds as $color => $wids) {
          $cid = $this->color_to_int($color);

          foreach ($wids as $wid) {
            if (isset($worlds[$wid])) {
              $worlds[$wid]['color'] = $cid;
              $worlds[$wid]['tier'] = $match->id;
            }

            if (isset($current[$wid]) && $current[$wid] != $match->id) {
              if (preg_match('/^1-/', $match->id))
                $na = TRUE;
              if (preg_match('/^2-/', $match->id))
                $eu = TRUE;
            }
          }

        }
      }
    }

    // The matchups changed
    if ($na)
      $this->query("DELETE FROM gw2_match_maps WHERE tier ~ '^1-'");
    if ($eu)
      $this->query("DELETE FROM gw2_match_maps WHERE tier ~ '^2-'");

    // Update the database
    $this->insert_many('gw2_match_maps', $maps, NULL, TRUE);
    $this->insert_many('gw2_matches', $matches, NULL, TRUE);
    $this->insert_many('gw2_worlds', $worlds, NULL, TRUE);
    return TRUE;
  }

  /****************************************************************************************
   * Updates the prices of all blc collections
   ****************************************************************************************/
  protected function upgrade_blc($full=FALSE) {
    // Get the achievement category for black lion collections
    $cat = $this->api->apiload('achievements/categories/76');
    if (!isset($cat, $cat->achievements))
      return FALSE;

    // Download the achievements
    $blcs = $items = [];
    foreach ($this->api->bulkload('achievements', $cat->achievements) as $ach) {
      if (!isset($ach->id, $ach->name, $ach->bits))
        continue;

      // Only achievements with skins
      $skins = FALSE;
      foreach ($ach->bits as $bit)
        if (isset($bit->type, $bit->id) && $bit->type === 'Skin') {
          $items[] = ['id' => $bit->id, 'blc_id' => $ach->id];
          $skins = TRUE;
        }

      if ($skins)
        $blcs[] = ['id' => $ach->id, 'name' => $ach->name];
    }

    // Insert the collections
    $this->insert_many('gw2_blc', $blcs, NULL, TRUE);
    $this->update_many('gw2_skins', $items);

    return TRUE;
  }

  /*****-***********************************************************************************
   * Returns if the tier is valid
   ****************************************************************************************/
  function has_tier($tier) {
    $query = "SELECT COUNT(*) FROM gw2_matches WHERE tier = $1";
    if ($row = $this->query($query, [$tier])->fetch())
      return $row[0] > 0;
    return FALSE;
  }

  /****************************************************************************************
   * Get the black lion collections
   ****************************************************************************************/
  function get_blc($count=NULL) {
    if ($this->config('order_blc', 'sell') == 'sell')
      $order = 'sell_price, buy_price';
    else
      $order = 'buy_price, sell_price';

    $query = "SELECT blc.name AS name, SUM(it.sell_price) AS sell_price, ".
      "SUM(it.buy_price) AS buy_price FROM \"gw2_blc\" AS blc ".
      "LEFT JOIN \"gw2_skins\" AS skin ON skin.blc_id = blc.id ".
      "LEFT JOIN \"gw2_item_skins\" AS its ON its.skin_id = skin.id ".
      "LEFT JOIN \"gw2_items\" AS it ON it.id = its.item_id GROUP BY blc.id ".
      "ORDER BY {$order}";

    $data = [];
    $res = $this->query($query);
    while (($row = $res->fetch_object()) && $count != 0)
      if ($row->sell_price > 0 && $row->buy_price > 0) {
        $data[] = $row;

        if (!is_null($count))
          $count--;
      }
    return $data;
  }

  /*****-***********************************************************************************
   * Returns the items with prices by ids
   ****************************************************************************************/
  function get($ids, $count=NULL) {
    if (is_null($count))
      $count = count($ids);

    if ($this->config('order', 'buy') == 'buy')
      $order = 'buy_price';
    else
      $order = 'sell_price';

    // Build the query
    $vals = [];
    $i = 1;
    foreach ($ids as $id) {
      $vals[] = "\${$i}";
      $i++;
    }
    $vals = implode(',', $vals);
    $query = "SELECT * FROM \"gw2_items\" WHERE id IN ({$vals}) ".
      "ORDER BY {$order} DESC LIMIT \${$i}";

    $ids[] = $count;
    $res = $this->query($query, $ids);

    // Extract the items
    $items = [];
    while ($row = $res->fetch_object())
      $items[] = $row;

    return $items;
  }

  /****************************************************************************************
   * Get the last items
   ****************************************************************************************/
  function get_last_items($count=10) {
    $data = [];
    $query = 'SELECT * FROM "gw2_items" ORDER BY timestamp DESC LIMIT $1';
    $res = $this->query($query, [$count]);
    while ($row = $res->fetch_object())
      $data[] = (object)$row;
    return $data;
  }

  /****************************************************************************************
   * Get the item ids from the watchlist
   ****************************************************************************************/
  function get_item_ids() {
    $groups = $this->config('groups');
    if (!isset($groups))
      return [];

    $ids = [];
    foreach ($groups as $items)
      $ids = array_merge($ids, $items);
    return $ids;
  }

  /****************************************************************************************
   * Get worlds
   ****************************************************************************************/
  function get_worlds() {
    $na = $this->config('region') === 'na';
    $query = "SELECT * FROM \"gw2_worlds\" WHERE tier ~ $1 ORDER BY tier, color, name";
    $res = $this->query($query, [$na ? '^1-' : '^2-']);
    $data = [];
    while ($row = $res->fetch_object())
      $data[] = $row;
    return $data;
  }

  /****************************************************************************************
   * Get matches
   ****************************************************************************************/
  function get_matches() {
    $na = $this->config('region') === 'na';
    $query = "SELECT * FROM \"gw2_matches\" WHERE tier ~ $1 ORDER BY tier, color";
    $res = $this->query($query, [$na ? '^1-' : '^2-']);
    $data = [];
    while ($row = $res->fetch_object()) {
      $row->color = $this->int_to_color($row->color);
      $data[] = $row;
    }
    return $data;
  }

  /****************************************************************************************
   * Get match
   ****************************************************************************************/
  function get_match($tier) {
    $query = "SELECT * FROM \"gw2_match_maps\" WHERE tier = $1 ORDER BY timestamp DESC, map, color";
    $res = $this->query($query, [$tier]);
    $data = [];
    while ($row = $res->fetch_object()) {
      $row->map = $this->int_to_map($row->map);
      $row->color = $this->int_to_color($row->color);
      $data[] = $row;
    }
    return $data;
  }

  /****************************************************************************************
   * Get continents
   ****************************************************************************************/
  function get_continents() {
    $query = "SELECT * FROM \"gw2_continents\"";
    $res = $this->query($query);
    $data = [];
    while ($row = $res->fetch_object()) {
      $row->id = (int)$row->id;
      $row->width = (int)$row->width;
      $row->height = (int)$row->height;
      $row->min_zoom = (int)$row->min_zoom;
      $row->max_zoom = (int)$row->max_zoom;
      $row->code = strtolower(trim($row->name));
      $data[] = $row;
    }
    return $data;
  }

  /****************************************************************************************
   * Get maps
   ****************************************************************************************/
  function get_maps($continent_id=FALSE) {
    $query = "SELECT * FROM \"gw2_maps\"";
    if ($continent_id) {
      $query .= "WHERE continent_id = $1";
      $res = $this->query($query, [$continent_id]);
    } else
      $res = $this->query($query);
    $conv = [
      'cleft', 'ctop', 'cright', 'cbottom',
      'mleft', 'mtop', 'mright', 'mbottom',
    ];
    $data = [];
    while ($row = $res->fetch_object()) {
      $row->id = (int)$row->id;
      $row->continent_id = (int)$row->continent_id;
      foreach ($conv as $key)
        $row->{$key} = (int)$row->{$key};

      $row->cwidth = $row->cright - $row->cleft;
      $row->cheight = $row->cbottom - $row->ctop;
      $row->mwidth = $row->mright - $row->mleft;
      $row->mheight = $row->mbottom - $row->mtop;
      $data[$row->id] = $row;
    }
    return $data;
  }

  /****************************************************************************************
   * Get objectives
   ****************************************************************************************/
  function get_objectives() {
    $query = "SELECT * FROM \"gw2_objectives\"";
    $res = $this->query($query);
    $data = [];
    while ($row = $res->fetch_object()) {
      $row->x = (float)$row->x;
      $row->y = (float)$row->y;
      $row->z = (float)$row->z;
      $data[] = $row;
    }
    return $data;
  }
};
