<!DOCTYPE html>
<html>
<head>
  <title>Index</title>
</head>

<?php
  require_once('modules/base.php');
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="container"></div>
</body>
</html>
