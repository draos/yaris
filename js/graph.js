function svg_child(svg, tag, attrs, text) {
  let ele = document.createElementNS("http://www.w3.org/2000/svg", tag);
  if (text)
    ele.appendChild(document.createTextNode(text || ''));

  for (let x in attrs)
    if (attrs.hasOwnProperty(x))
      ele.setAttributeNS(null, x, attrs[x]);

  svg.appendChild(ele);
  return ele;
}

function round_to(x, d) {
  if (!d)
    d = 10;
  return Math.round(d * x) / d;
}

class Graph {
  constructor(container, height, left, data, bounds) {
    this.container = container;
    this.data = data;
    this.bounds = bounds;
    this.height = height;
    this.left = left;
    this.marks = [];
    this.unit_mapping = [];
    this.time_range = this.index = 0;
  }

  set_marks(marks) {
    this.marks = marks;
  }

  set_unit_mapping(unit_mapping) {
    this.unit_mapping = unit_mapping;
  }

  set_time_range(a, b) {
    this.time_range = Math.abs(b - a);
  }

  generate_polyline(svg, type) {
    var points = [];
    var x, y;
    for (let item of this.data) {
      x = svg.graph.left + svg.graph.width * item.x;
      y = svg.graph.top + this.height * (1.0 - item[type]);
      points.push(x + ',' + y);
    }
    svg_child(svg, 'polyline', {points: points.join(' ')});
  }

  generate_horizontal(svg, rel_y, class_name) {
    if (0 <= rel_y && rel_y <= 1) {
      let y = svg.graph.top + this.height * (1.0 - rel_y);
      svg_child(svg, 'line', {
        x1: svg.graph.left, x2: svg.graph.left + svg.graph.width,
        y1: y, y2: y, class: class_name,
      });
    }
  }

  generate_vertical(svg, rel_x, class_name) {
    if (0 <= rel_x && rel_x <= 1) {
      let x = svg.graph.left + svg.graph.width * rel_x;
      svg_child(svg, 'line', {
        x1: x, x2: x, y1: svg.graph.top, y2: svg.graph.top + this.height,
        class: class_name,
      });
    }
  }

  generate_text(svg, rel_x, rel_y, text, attrs) {
    if (!attrs)
      attrs = [];

    attrs.x = svg.graph.left + svg.graph.width * rel_x;
    attrs.y = svg.graph.top + this.height * (1.0 - rel_y);
    svg_child(svg, 'text', attrs, text + '');
  }

  generate_mark(svg, mark, type) {
    this.generate_vertical(svg, mark, type);

    let symbol = false;
    switch (type) {
      case 'noon':
        symbol = "\u2600";
        break;
      case 'midnight':
        symbol = "\u263d";
        break;
    }

    if (symbol)
      this.generate_text(svg, mark, 1, symbol, {dy: -10, 'text-anchor': 'middle', class: 'mark'});
  }

  create_symbol(container, type) {
    let symbol = document.createElement('span');
    switch (type) {
      case 'qpf':
        symbol.setAttribute('class', 'wi wi-umbrella');
        break;
      case 'cloudiness':
        symbol.setAttribute('class', 'wi wi-cloud');
        break;
      case 'wind_speed':
        symbol.setAttribute('class', 'wi wi-wind');
        break;
      default:
        symbol.setAttribute('class', 'wi wi-' + type);
        break;
    }

    container.appendChild(symbol);
  }

  generate_svg(type, attributes) {
    let wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'wrapper');
    container.appendChild(wrapper);

    let desc = document.createElement('div');
    desc.setAttribute('class', 'symbol');
    wrapper.appendChild(desc);
    this.create_symbol(desc, type);

    let unit = document.createElement('span');
    unit.innerHTML = ' ' + this.unit_mapping[type] || '';
    desc.appendChild(unit);

    let svg = svg_child(wrapper, 'svg', attributes);

    svg.graph = {
      parent: wrapper,
      type: type,
      index: this.index++,
      unit: this.unit_mapping[type] || '',
      left: this.left,
      top: svg.clientHeight - this.height - 10,
      width: svg.clientWidth - this.left,
      height: svg.clientHeight - 2 * this.height,
    };
    let self = this;
    svg.addEventListener("click", function(){self.svg_clicked(svg);})

    this.fill_svg(svg);
  }

  fill_svg(svg) {
    this.generate_horizontal(svg, 0.0, 'min');
    this.generate_horizontal(svg, 0.5, 'avg');
    this.generate_horizontal(svg, 1.0, 'max');

    if (this.marks)
      for (let mark of this.marks)
        this.generate_mark(svg, mark[0], mark[1]);

    this.generate_polyline(svg, svg.graph.type);

    let attrs = {'text-anchor': 'end', dx: -10, dy: 5};
    let [min, max] = this.bounds[svg.graph.type];
    if (min < max) {
      this.generate_text(svg, 0, 1.0, round_to(max), attrs);
      this.generate_text(svg, 0, 0.0, round_to(min), attrs);
    }
    this.generate_text(svg, 0, 0.5, round_to(0.5 * (min + max)), attrs);
    attrs.dy = -15;
  }

  generate(types, attributes) {
    this.types = types;
    for (let type of types)
      this.generate_svg(type, attributes);
  }

  svg_clicked(svg) {
    let dialog = document.getElementById('dialog');
    if (!dialog)
      return;

    let tmp = this.types.slice();
    let index = svg.graph.index;
    for (let ele of dialog.children) {
      tmp[index] = ele.type;
      ele.setAttribute('href', window.location.pathname + '?types=' + tmp.join(','));
    }
    dialog.setAttribute('class', '');
    dialog.parent_svg = svg;
    svg.graph.parent.appendChild(dialog);
    setTimeout(function() {dialog.setAttribute('class', 'hidden');}, 2000);
  }
}
