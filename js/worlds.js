class WvWMap {
  constructor(objectives, container, options) {
    this.objectives = objectives;
    this.options = options;
    this.markers = [];

    this.init_map(container);
    this.api_objectives(this.options.fetch_url);

    let self = this;
    setInterval(function(){self.update_timers();}, 1000);
  }

  init_map(container) {
    this.map = L.map(container, {
      minZoom: this.options.min_zoom, maxZoom: this.options.max_zoom,
      crs: L.CRS.Simple,
    });

    let nw = this.unproject([this.options.x_min, this.options.y_min]);
    let se = this.unproject([this.options.x_max, this.options.y_max]);
    let bounds = new L.LatLngBounds(nw, se);

    L.tileLayer(this.options.url).addTo(this.map);
    this.map.fitBounds(bounds);

    for (let obj of objectives) {
      if (this.options.visible.indexOf(obj.type) > -1) {
        let icon = new L.divIcon({
          className: 'marker obj-' + obj.id,
          iconSize: [32, 32],
          iconAnchor: [16, 16],
          html: '<img src="' + obj.marker + '" width="32" height="32"><div/>',
        });
        let marker = L.marker(this.unproject([obj.x, obj.y]), {icon: icon});
        this.markers[obj.id] = marker;
        marker.addTo(this.map);
      }
    }
  }

  update_timers() {
    let now = new Date();
    for (let obj_id in this.markers) {
      let marker = this.markers[obj_id];
      if (!marker.last_flipped)
        continue;

      let time = now - (new Date(marker.last_flipped));
      let secs = 300 - time / 1000;
      if (!marker.visible && secs > 0)
        marker.visible = true;

      let ele = marker.getElement();
      if (marker.visible && ele) {
        let min = Math.floor(secs / 60);
        let sec = Math.floor(secs) % 60;
        if (secs < 1) {
          ele.children[1].innerHTML = '';
          marker.visible = false;
        } else if (sec < 10)
          ele.children[1].innerHTML = min + ':0' + sec;
        else
          ele.children[1].innerHTML = min + ':' + sec;
      }
    }
  }

  project(coord) {return this.map.project(coord, this.options.max_zoom);}
  unproject(coord) {return this.map.unproject(coord, this.options.max_zoom);}

  async api_objectives(fetch_url) {
    const response = await fetch(fetch_url);
    const data = await response.json();
    if (data.maps)
      for (let map of data.maps)
        for (let obj of map.objectives) {
          let marker = this.markers[obj.id];
          if (marker) {
            marker.last_flipped = obj.last_flipped;
            let ele = marker.getElement();
            if (ele)
              ele.children[0].setAttribute('class', obj.owner.toLowerCase());
          }
        }
  }
}
