<!DOCTYPE html>
<html>
<meta http-equiv="refresh" content="3600"/>
<head>
  <title>Weather Map</title>
</head>

<?php
  require_once('modules/openweathermap.php');

  $module = new OpenWeatherModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);
  $city = $module->city();
  $coord = number_format($city->latitude, 4).','.number_format($city->longitude, 4);

  switch ($_GET['mode'] ?? FALSE) {
    case 'cloudiness': case 'pressure': case 'qpf': case 'temp': case 'wind_speed':
      $mode = $_GET['mode'];
      break;
    default:
      $mode = FALSE;
      break;
  }

  switch ($mode) {
    case 'cloudiness': $layer = 'clouds'; break;
    case 'qpf': $layer = 'precipitation'; break;
    case 'wind_speed': $layer = 'wind'; break;
    default: $layer = $mode; break;
  }

  $legend = [
    'cloudiness' => [
      100 => [240, 240, 255, 1.0], 90 => [242, 241, 255, 1.0], 80 => [243, 242, 255, 1.0],
      70 => [244, 244, 255, 1.0], 60 => [246, 245, 255, 0.75], 50 => [247, 247, 255, 0.5],
      40 => [249, 248, 255, 0.4], 30 => [250, 250, 255, 0.3], 20 => [252, 251, 255, 0.2],
      10 => [253, 253, 255, 0.1], 0 => [255, 255, 255, 0.0],
    ],
    'qpf' => [
      '140.0' => [20, 20, 255, 0.9], '10.0' => [80, 80, 225, 0.7], '1.0' => [110, 110, 205, 0.7],
      '0.5' => [120, 120, 190, 0.7], '0.2' => [150, 150, 170, 0], '0.1' => [200, 150, 150, 0],
      '0.0' => [225, 200, 100, 0],
    ],
    'temp' => [
      30 => [252, 128, 20, 1.0], 25 => [255, 194, 40, 1.0], 20 => [255, 240, 40, 1.0],
      10 => [194, 255, 40, 1.0], 0 => [35, 221, 221, 1.0], -10 => [32, 196, 232, 1.0],
      -20 => [32, 140, 236, 1.0], -30 => [130, 87, 219, 1.0], -40 => [130, 22, 146, 1.0],
      -45 => [130, 22, 146, 1.0], -55 => [130, 22, 146, 1.0], -65 => [130, 22, 146, 1.0],
    ],
    'pressure' => [
      1080 => [198, 0, 0, 1.0], 1060 => [243, 54, 59, 1.0], 1040 => [251, 85, 21, 1.0],
      1020 => [240, 184, 0, 1.0], 1010 => [176, 247, 32, 1], 1000 => [141, 231, 199, 1.0],
      980 => [75, 208, 214, 1.0], 960 => [0, 170, 255, 1.0], 940 => [0, 115, 255, 1.0],
    ],
    'wind_speed' => [
      720 => [13, 17, 38, 1.0], 360 => [70, 0, 175, 1.0], 180 => [116, 76, 172, 0.9],
      90 => [63, 33, 59, 0.8], 54 => [179, 100, 188, 0.7], 18 => [238, 206, 206, 0.4],
      '3.6' => [255, 255, 255, 0.0],
    ],
  ];
?>

<?php build_styles()?>
<link rel="stylesheet" href="css/leaflet.css"/>
<script src="js/leaflet.js"></script>

<style>
<?php switch ($mode) {
  case 'cloudiness': case 'qpf': case 'temp': case 'pressure': case 'wind_speed':
    echo '#legend {background: linear-gradient(';
    $data = [];
    foreach ($legend[$mode] as $rgba)
      $data[] = 'rgba('.implode(',', array_map(number_format, $rgba)).')';
    echo implode(',', $data);
    echo ')}';
    break;
  }
?>
</style>

<body>
<?php build_menu();?>

  <div id="submenu">
    <a class="wi wi-temperature" href="map.php?mode=temp"></a>
    <a class="wi wi-qpf" href="map.php?mode=qpf"></a>
    <a class="wi wi-clouds" href="map.php?mode=cloudiness"></a>
    <a class="wi wi-wind" href="map.php?mode=wind_speed"></a>
    <a class="wi wi-pressure" href="map.php?mode=pressure"></a>
  </div>

  <div id="container">
    <div id="map"></div>

<?php
  switch ($mode) {
    case 'cloudiness': case 'qpf': case 'temp': case 'pressure': case 'wind_speed':
      $units = $module->get_unit_mapping();

      echo "    <table id=\"legend\">\n";
      foreach ($legend[$mode] as $stop => $rgba)
        echo "    <tr><td>{$stop}{$units[$mode]}</td></tr>\n";
      echo "    </table>\n";
      break;
  }
?>
  </div>

  <script>
    var map = L.map('map').setView([<?php echo $coord;?>], 5);
    L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {maxZoom: 10, minZoom: 4}
    ).addTo(map);

<?php if ($layer) {?>
    var api_key = '<?php echo $module->config("api_key");?>';
    var layer = <?php echo "'{$layer}_new'";?>;
    L.tileLayer(
      'https://tile.openweathermap.org/map/{layer}/{z}/{x}/{y}.png?appid={api_key}',
      {maxZoom: map.getMaxZoom(), minZoom: map.getMinZoom(), api_key: api_key, layer: layer}
    ).addTo(map);
<?php } ?>
  </script>
</body>
</html>
