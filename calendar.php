<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="refresh" content="3600"/>
  <title>Calendar</title>
</head>

<?php
  require_once('modules/calendar.php');
  $module = new \CalendarModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);
  $events = $module->get();
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

 <div id="container">
  <table id="calendar">
<?php
  foreach ($events as $event) {?>
   <tr>
    <td class="date"><?php
    echo $event->start->format($event->type == 'date' ? 'd.m.Y' : 'H:i d.m.Y');
?>
    </td>
    <td class="title"><?php echo $event->title;?></td>
   </tr>
<?php }?>
  </table>
 </div>
</body>
</html>
