<!DOCTYPE html>
<html>
<!-- <meta http-equiv="refresh" content="60"/> -->
<head>
  <title>Worlds</title>
</head>
<?php
  require_once('modules/guildwars2.php');

  function find_best($times, $target, $threshold=3600) {
    $best = NULL;
    $best_time = '';
    foreach ($times as $secs => $time) {
      $d = abs($target - $secs);
      if ($d <= $threshold && (is_null($best) || $d < $best)) {
        $best = $d;
        $best_time = $time;
      }
    }
    return $best_time;
  }

  // Build the api object
  $module = new \Gw2Module();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);

  if (isset($_GET['tier']) && $module->has_tier($_GET['tier'])) {
    $cur_tier = $_GET['tier'];
    $map = (bool)$_GET['map'] ?? FALSE;
  } else
    $map = $cur_tier = NULL;
?>

<?php if ($map) {?>
<link rel="stylesheet" href="css/leaflet.css"/>
<script src="js/leaflet.js"></script>
<script src="js/worlds.js"></script>
<?php }?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="submenu">
    <a class="fas fa-minus" href="watchlist.php"></a>
    <a class="fas fa-bars" href="watchlist.php?mode=blc"></a>
    <a class="fas fa-table" href="worlds.php"></a>
<?php if ($cur_tier && $map) {?>
    <a class="fas fa-stream" href="worlds.php?tier=<?php echo $cur_tier;?>"></a>
<?php } else if ($cur_tier) {?>
    <a class="fas fa-map" href="worlds.php?map=1&tier=<?php echo $cur_tier;?>"></a>
<?php }?>
  </div>

<?php if ($map) {?>
  <div id="container">
    <div id="map"/>
  </div>
  <script>
    var objectives = <?php echo json_encode($module->get_objectives());?>;
    new WvWMap(objectives, 'map', {
      fetch_url: "https://api.guildwars2.com/v2/wvw/matches/<?php echo $cur_tier;?>",
      max_zoom: 6, min_zoom: 2,
      url: 'https://tiles.guildwars2.com/2/1/{z}/{x}/{y}.jpg',
      visible: ['Castle', 'Camp', 'Keep', 'Tower'],
      x_min: 5630, x_max: 15358, y_min: 8958, y_max: 15870,
    });
  </script>
<?php } else {?>
  <div id="container">
    <table id="matches">
<?php
  $matches = [];
  foreach ($module->get_matches() as $match)
    $matches[$match->tier][] = $match;

  $worlds = [];
  foreach ($module->get_worlds() as $world)
    $worlds[$world->tier][$world->color][] = preg_replace('/\[.*\]/', '', $world->name);

  foreach ($worlds as $tid => $tier) {?>
      <tr class="match">
<?php
    foreach (array_map(null, $tier, $matches[$tid]) as $group) {
      $names = $group[0];
      $data = $group[1];
      if (is_null($cur_tier) || $cur_tier === $data->tier) {?>
        <td class="team <?php echo $data->color;?>">
<?php if (is_null($cur_tier)) {?>
          <a href="worlds.php?tier=<?php echo $data->tier;?>"></a>
<?php } else {?>
          <a href="worlds.php?tier=<?php echo $data->tier;?>&map=1"></a>
<?php }?>
          <div class="name"><?php echo implode('<br>', $names);?></div>
          <div class="data">
            <span class="victories">VP: <?php echo $data->victories;?></span>
            <span class="kd">KD: <?php echo round($data->deaths > 0 ? $data->kills / $data->deaths : 0, 2);?></span>
          </div>
        </td>
<?php }}?>
      </tr>
<?php }?>
    </table>
<?php
  if (!is_null($cur_tier)) {
    $data = $times = [];
    $utc = new \DateTimeZone('UTC');
    $now = time();
    foreach ($module->get_match($cur_tier) as $map) {
      $data[$map->map][$map->color][$map->timestamp] = (object)[
        'deaths' => $map->deaths, 'kills' => $map->kills,
      ];

      $dt = new \DateTime($map->timestamp, $utc);
      $times[$now - $dt->getTimestamp()] = $map->timestamp;
    }

    $three = find_best($times, 3 * 60 * 60);
    $day = find_best($times, 24 * 60 * 60); ?>
    <table id="maps">
<?php
    foreach ($data as $map => $mdata) {
      foreach (['Total', '3h', '24h'] as $k => $text) {?>
      <tr>
        <td class="map">
          <span class="map-name"><?php if (!$k) echo ucfirst($map);?></span>
          <span class="map-time"><?php echo $text;?></span>
        </td>
<?php
        foreach ($mdata as $color => $cdata) {
          if (count($cdata)) {
            $first = array_values($cdata)[0];
            $value = '';
            switch ($k) {
              case 0:
                if ($first->deaths > 0)
                  $value = sprintf("%.2f", $first->kills / $first->deaths);
                break;
              case 1:
                if (isset($cdata[$three])) {
                  $d = $cdata[$three];
                  $kills = $first->kills - $d->kills;
                  $deaths = $first->deaths - $d->deaths;
                  if ($deaths > 0)
                    $value = sprintf("%.2f", $kills / $deaths);
                }
                break;
              case 2:
                if (isset($cdata[$day])) {
                  $d = $cdata[$day];
                  $kills = $first->kills - $d->kills;
                  $deaths = $first->deaths - $d->deaths;
                  if ($deaths > 0)
                    $value = sprintf("%.2f", $kills / $deaths);
                }
                break;
            }?>
        <td class="<?php echo $color;?>">
          <span class="kd"><?php echo $value;?></span>
        </td>
<?php }}?>
      </tr>
<?php }}?>
    </table>
  </div>
<?php }}?>
</body>
</html>
