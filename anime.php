<!DOCTYPE html>
<html>
<meta http-equiv="refresh" content="60"/>
<head>
  <title>Anime</title>
</head>

<?php
  require_once('modules/anime.php');

  $module = new \AnimeModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);

  $mode = NULL;
  switch ($_GET['mode'] ?? '') {
    case 'anime':
      $mode = $_GET['mode'];
      break;
  }
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="submenu">
    <a class="fas fa-sort-amount-down" href="anime.php"></a>
    <a class="fas fa-sort-alpha-down" href="anime.php?mode=anime"></a>
  </div>

  <div id="container">
    <table id="animes">
<?php if ($mode === 'anime') {
  foreach ($module->get_animes() as $anime) {?>
    <tr><td class="anime"><?php echo $anime;?></td></tr>
<?php }} else {
  echo "      <col width=\"25%\"/><col width=\"10%\"/><col width=\"65%\"/>\n";
  foreach ($module->get_latest() as $item) {?>
    <tr>
      <td class="anime"><?php echo $item->anime;?></td>
      <td class="lang"><?php echo "[{$item->lang}]";?></td>
      <td class="episode"><?php echo $item->episode;?></td>
    </tr>
<?php }}?>
    </table>
  </div>
</body>
</html>
