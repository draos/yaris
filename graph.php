<!DOCTYPE html>
<html>
<?php
  $refresh = 3600 - 60 * idate('i') - idate('s');
?>
<meta http-equiv="refresh" content="<?php echo $refresh;?>"/>
<head>
  <title>Graphs</title>
</head>
<?php
  require_once('modules/openweathermap.php');

  $module = new OpenWeatherModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);
  $hours = $module->config('next_hours', 72);
  $city = $module->city();
  $start = time();

  $data = $module->get_points($start, $hours);
  $marks = $module->get_specific_points($start, $hours);
  $mapping = $module->get_unit_mapping();

  // Use a default configuration
  if (isset($_GET['types']) && $_GET['types']) {
    $types = $_GET['types'].',temp,qpf,cloudiness';
    // Don't show duplicates
    $types = array_unique(explode(',', $types));
    // Only valid options
    $types = array_intersect($types, array_keys($mapping));
    $types = array_slice(array_values($types), 0, 3);
    $module->set_option('last_visit', implode(',', $types));
  } else
    $types = explode(',', $module->get_option('last_visit', 'temp,qpf,cloudiness'));
?>

<script src="js/graph.js"></script>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="location">
<?php if (isset($city->country, $city->name)) {?>
    <span class="country"><?php echo $city->country;?></span>
    <span class="city"><?php echo $city->name;?></span>
<?php }?>
  </div>

  <div id="dialog" class="hidden">
    <a class="type wi wi-temp" type="temp"></a>
    <a class="type wi wi-umbrella" type="qpf"></a>
    <a class="type wi wi-cloud" type="cloudiness"></a>
    <a class="type wi wi-wind" type="wind_speed"></a>
    <a class="type wi wi-pressure" type="pressure"></a>
    <a class="type wi wi-humidity" type="humidity"></a>
    <a class="type wi wi-rain" type="rain"></a>
    <a class="type wi wi-snow" type="snow"></a>
  </div>

  <div id="container">
    <div id="graphs"></div>
  </div>

  <script>
    let data = <?php echo json_encode($data[0]);?>;
    let bounds = <?php echo json_encode($data[1]);?>;
    let marks = <?php echo json_encode($marks);?>;
    let mapping = <?php echo json_encode($mapping);?>;
    let types = <?php echo json_encode($types);?>;
    let container = document.getElementById('graphs');
    let attributes = {width: '100%', height: '27vh'};

    let g = new Graph(container, 80, 70, data, bounds);
    g.set_marks(marks);
    g.set_unit_mapping(mapping);
    g.generate(types, attributes);
  </script>
</body>
</html>
