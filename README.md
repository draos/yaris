# Yet Another Raspberry Info Screen

### Modules:
* Anime:
  * Information about the latest animes episodes
  * External update script (not included)
* Calendar:
  * Shows the next events of a calendar
  * Using a CalDAV server
* [Guild Wars 2](https://guildwars2.com):
  * Shows different overview using the API
  * Short watchlist for items
  * Watchlist of the cheapest to finish Black Lion Weapon Collections
  * Overview over the WvW matches
* [OpenWeatherMap](https://openweathermap.org):
  * Shows weather informations in different views
  * Map view with the different layers offered by OpenWeatherMap
  * Clear forecast for the next days
  * Graphs with the trends of the next hours
* [pwned](https://haveibeenpwned.com):
  * Shows breaches your account is in
  * Daily update using the API by haveibeenpwned
* [xkcd](https://xkcd.com):
  * View the latest and past comics

### Requirements:
* Display for the Raspberry Pi
* Local webserver with php (e.g. nginx)
* Postgres SQL server
* Webbrowser with kiosk mode (e.g. chromium)

### Using:
* [Font Awesome](https://fontawesome.com)
* [Leaflet](https://leafletjs.com)
* [Weather Icons](https://erikflowers.github.io/weather-icons)

### Installation:
0. Clone the repository and initialize the submodules
1. Install and configure nginx with php
```$ apt install nginx php-fpm postgresql unclutter php-curl php-xml chromium-browser```
2. Creation of the file */boot/xinitrc* with following content
```
#!/bin/sh

unclutter &

while true; do
  # Clean up stuff
  rm -rf /home/pi/.cache;
  rm -rf /home/pi/.config;
  rm -rf /home/pi/.pki;

  # Set some display options
  xset s off
  xset -dpms
  xset s noblank

  # Start the kiosk mode
  chromium-browser --icognito --disable-pinch --overscroll-history-navigation=0 --kiosk --app=http:/localhost/
done
```
4. Adding of the following lines in */etc/rc.local*
```
if [ -f /boot/xinitrc ]; then
  ln -fs /boot/xinitrc /home/pi/.xinitrc
  su -l pi -c 'startx' &
fi
```
5. Configure a postgres user with password and full rights on the used database
6. Register and get the API key from [OpenWeatherMap](https://openweathermap.org)
7. Configure the infoscreen via the *settings.php*
 * Generate a secure cron token. `hexdump -v -e '1/1 "%02X"' -n 24 /dev/urandom`
 * Set the required database parameters
 * Activate the wanted module and set the parameter marked as required
