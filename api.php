<?php
  require_once('modules/base.php');

  $module = new \BaseModule();
  $module->upgrade();

  $path = $_GET['path'] ?? FALSE;
  $params = $_GET;
  $token = trim($_SERVER['HTTP_AUTHORIZATION'] ?? '');

  try {
    echo json_encode($module->handle_request($token, $params));
  } catch (Exception $e) {
    $msg = (object)["status" => "error", "error" => $e->getMessage()];

    if (isset($e->endpoints) && !is_null($e->endpoints))
      $msg->endpoints = $e->endpoints;

    echo json_encode($msg);
  }
?>