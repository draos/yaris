<!DOCTYPE html>
<html>
<meta http-equiv="refresh" content="60"/>
<head>
  <title>Watchlist</title>
</head>
<?php
  require_once('modules/guildwars2.php');

  // Build the module object
  $module = new \Gw2Module();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);

  $groups = $blcs = NULL;
  switch (isset($_GET['mode']) && $_GET['mode']) {
    case 'blc':
      $blcs = TRUE;
      $maximum = $module->config('maximum_blc');
      break;
    default:
      $maximum = $module->config('maximum');
      $groups = $module->config('groups');
      break;
  }

  if (isset($groups)) {
    $counter = 0;
    echo "<style>\n";
    foreach ($groups as $group => $ids) {
      if ($counter > 0)
        echo "tr:nth-child({$counter}) {border-bottom: 1px solid white}\n";
      $counter += is_null($maximum) ? count($ids) : min($maximum, count($ids));
    }
    echo "</style>\n";
  }
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="submenu">
    <a class="fas fa-minus" href="watchlist.php"></a>
    <a class="fas fa-bars" href="watchlist.php?mode=blc"></a>
    <a class="fas fa-globe-europe" href="worlds.php"></a>
  </div>

  <div id="container">
    <table id="watchlist">
<?php
    if (isset($groups))
      foreach ($groups as $group => $ids) {
        $items = $module->get($ids, $maximum);
        foreach ($items as $item) {?>
      <tr>
        <td class="icon"><img width="32px" height="32px" src="<?php echo $item->icon;?>"></td>
        <td class="name"><?php echo $item->name;?></td>
        <td class="buy"><?php echo convert($item->buy_price);?></td>
        <td class="sell"><?php echo convert($item->sell_price);?></td>
      </tr>
<?php }}
  if (isset($blcs))
    foreach ($module->get_blc($maximum) as $blc) {?>
      <tr>
        <td class="name"><?php echo $blc->name;?></td>
        <td class="buy"><?php echo convert($blc->buy_price);?></td>
        <td class="sell"><?php echo convert($blc->sell_price);?></td>
      </tr>
<?php }?>
    </table>
  </div>
</body>
</html>
