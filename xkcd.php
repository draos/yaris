<!DOCTYPE html>
<html>
<head>
  <title>xkcd</title>
</head>
<?php
  require_once('modules/xkcd.php');

  $module = new \XKCDModule();
  $module->set_last_site($_SERVER['DOCUMENT_URI'] ?? FALSE);
  $last = $module->last_id();

  $id = isset($_GET['id']) ? $_GET['id'] : FALSE;
  $data = $module->get($id === 'random' ? random_int(1, $last) : $id);
  $src = $data->cached ? $data->img : $data->url;
?>

<?php build_styles()?>
<body>
<?php build_menu();?>

  <div id="submenu">
    <a href="xkcd.php?id=1">
      <span class="fas fa-angle-double-left"></span>
    </a>
<?php if ($data) {?>
    <a href="xkcd.php?id=<?php echo $data->id - 1;?>">
      <span class="fas fa-angle-left"></span>
    </a>
<?php }?>
    <a href="xkcd.php?id=random">
      <span class="fas fa-random"></span>
    </a>
<?php if ($data) {?>
    <a href="xkcd.php?id=<?php echo $data->id + 1;?>">
      <span class="fas fa-angle-right"></span>
    </a>
<?php }?>
    <a href="xkcd.php?id=<?php echo $last;?>">
      <span class="fas fa-angle-double-right"></span>
    </a>
    <span id="test"></span>
  </div>

  <div id="container" class="xkcd-container">
    <div class="title"><?php echo $data->title;?></div>
    <img src="<?php echo $src;?>" id="xkcd-image">
    <div class="description"><?php echo $data->description;?></div>
  </div>
</body>
</html>
